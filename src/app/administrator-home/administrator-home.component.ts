import { Component, OnInit, ɵConsole } from '@angular/core';
import { AdministratorService } from '../services/administrator.service';
import { AuthorizationService } from '../services/authorization.service';
import { Administrator } from '../interfaces/administrator';
import { AngularFirestoreDocument, QueryDocumentSnapshot } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ResidenceService } from '../services/residence.service';
import { Residence } from '../interfaces/residence';
import { ResidenceTypeService } from '../services/residence-type.service';
import { ResidenceType } from '../interfaces/residenceType';
import { AdministratorConfiguration } from '../interfaces/administratorConfiguration';
import { AdministratorConfigurationService } from '../services/administrator-configuration.service';
import { Poll } from '../interfaces/poll';
import { PollService } from '../services/poll.service';
import { ToastService } from '../services/toast.service';

@Component({
  selector: 'app-administrator-home',
  templateUrl: './administrator-home.component.html',
  styleUrls: ['./administrator-home.component.scss']
})
export class AdministratorHomeComponent implements OnInit {
  public admin: any = null;
  public uid: string;
  public residence: any = [];
  residenceType: ResidenceType[];
  public code = '';
  public randomCode;
  private me: any = null;
  public loading = 'Cargando información...';
  public frmNewResidence: FormGroup;
  public adminInfo: AngularFirestoreDocument<firebase.firestore.DocumentData>;
  constructor(
    private adminService: AdministratorService,
    private residenceTypeService: ResidenceTypeService,
    private auth: AuthorizationService,
    private fb: FormBuilder,
    private residenceService: ResidenceService,
    private configurationService: AdministratorConfigurationService,
    private pollService: PollService,
    private toastService: ToastService
  ) {
    this.getAdministrationInfo();
  }

  ngOnInit() {
    this.frmNewResidence = this.createSignupForm();
    this.randomCode = this.generateRandomCode();
  }

  private generateRandomCode() {
    return (Math.random() * 1000).toFixed(0);
  }

  create() {
    const CONFIGURATION: AdministratorConfiguration = {
      visitor: { documentImage: true, identificationDocument: true, badgeNumber: true, email: true, phone: true },
      register: {
        residenceImage: true,
        residenceNumber: true,
        streetNumber: true,
        phone: true,
        listStreet: { active: false }
      }
    };
    const residence: Residence = {
      name: this.frmNewResidence.controls['name'].value,
      code: this.code,
      type: this.frmNewResidence.controls['type'].value,
      membership: 0,
      status: 1,
      configuration: CONFIGURATION
    };
    // console.log(residence, this.admin.uid);
    this.residenceService.set(residence, this.admin.uid).then(result1 => {
      this.frmNewResidence.controls['name'].setValue('');
      this.frmNewResidence.controls['type'].setValue('');
      this.frmNewResidence.controls['membership'].setValue('');
      this.code = '';
      document.getElementById('tabNew').classList.remove('active');
      document.getElementById('newPanel').classList.remove('active');
      document.getElementById('newPanel').classList.remove('show');
      document.getElementById('newPanel').classList.add('fade');
      this.randomCode = this.generateRandomCode();
    });
  }

  createSignupForm(): FormGroup {
    return this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(50)])],
      type: [null, Validators.compose([Validators.required])],
      membership: [null, null]
    });
  }

  onKey(event: any) {
    // without type info
    try {
      const residenceName = event.target.value.split(' ');
      this.code = '';
      if (residenceName.length === 1) {
        this.code = residenceName[0].substring(0, 6).toUpperCase();
      } else {
        residenceName.forEach(element => {
          this.code += element.substring(0, 3).toUpperCase();
        });
      }
    } catch (error) {
      this.code = event.target.value.toUpperCase();
    }
    this.code += this.randomCode;
  }

  private getAdministrationInfo() {
    this.auth.getUID().then(response => {
      this.adminInfo = this.adminService.get(response.uid);
      this.uid = response.uid;
      this.residenceTypeService
        .getAll()
        .valueChanges()
        .subscribe((data: ResidenceType[]) => {
          // console.log(data);
          this.residenceType = data;
          // this.residenceTypeService = result;
          // result.docs.forEach(i => console.log(i.get()));
        });
      // residences
      this.adminInfo
        .collection('residence', ref => ref.orderBy('name'))
        .snapshotChanges()
        .subscribe(f => {
          this.me = f;
          this.residence = [];
          // console.log('residence', this.residence);
          this.me.map(g => {
            this.residence.push({
              id: g.payload.doc.id,
              guards: 0,
              residents: 0,
              residentsPending: 0,
              residentsMissing: 0,
              // guards: this.me.collection('guard').snapshotChanges().subscribe(h => {
              //   return h.length;
              // }),
              data: g.payload.doc.data()
            });
            // console.log(this.residence);
            // console.log('residence2', g.payload.doc.data());
          });
          this.residence.forEach(element => {
            this.adminInfo
              .collection('residence')
              .doc(element.id)
              .collection('guard', ref => ref.where('status', '==', 1))
              .get()
              .subscribe(guard => (element.guards = guard.docs.length));
            this.adminInfo
              .collection('residence')
              .doc(element.id)
              .collection('resident', ref => ref.where('status', '==', 1))
              .get()
              .subscribe(residentPending => (element.residentPending = residentPending.docs.length));
            this.adminInfo
              .collection('residence')
              .doc(element.id)
              .collection('resident', ref => ref.where('status', '==', 0))
              .get()
              .subscribe(residentMissing => (element.residentMissing = residentMissing.docs.length));
            this.adminInfo
              .collection('residence')
              .doc(element.id)
              .collection('resident', ref => ref.where('status', '<', 99))
              .get()
              .subscribe(resident => {
                element.resident = resident.docs.length;
              });
          });
        });
      this.admin = this.adminInfo.valueChanges().subscribe(result => {
        this.admin = { ...result };
        // console.log(this.admin);
      });
    });
  }

  poll(residence, role, option, like) {
    const POLL: Poll = { role, option, like, reference: 'administrator/' + this.uid + '/residence/' + residence };
    this.pollService.set(POLL).then(result => {
      this.toastService.show('Thanks!', { classname: 'bg-success text-light my-5', delay: 5000 });
    });
  }
}
