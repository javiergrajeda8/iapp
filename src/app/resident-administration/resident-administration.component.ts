import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Administrator } from '../interfaces/administrator';
import { AdministratorService } from '../services/administrator.service';
import { ActivatedRoute } from '@angular/router';
import { AuthorizationService } from '../services/authorization.service';
import { CardService } from '../services/card.service';
import { ToastService } from '../services/toast.service';
import { CC } from '../interfaces/cc';
import { Resident } from '../interfaces/resident';
import { Residence } from '../interfaces/residence';
import { ResidentService } from '../services/resident.service';
import { ResidenceService } from '../services/residence.service';

@Component({
  selector: 'app-resident-administration',
  templateUrl: './resident-administration.component.html',
  styleUrls: ['./resident-administration.component.scss']
})
export class ResidentAdministrationComponent implements OnInit {
  public frmPlan: FormGroup;
  public frmAdministration: FormGroup;
  administratorID;
  residenceID;
  residentID;
  resident: Resident;
  residence: Residence;
  administrator: Administrator;
  changePassword = true;
  provider = '';
  constructor(
    private administratorService: AdministratorService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private auth: AuthorizationService,
    private cardService: CardService,
    private toastService: ToastService,
    private residentService: ResidentService,
    private residenceService: ResidenceService
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.residentID = this.activatedRoute.snapshot.params[param3];
  }

  ngOnInit() {
    this.frmPlan = this.createSignupForm();
    this.frmAdministration = this.createAdministratorForm();
    // this.getAdministratorInfo(this.administratorID);
    this.verificateUserProvider();
    this.getResidenceInfo(this.administratorID, this.residenceID);
    this.getResidentInfo(this.administratorID, this.residenceID, this.residentID);
  }

  getResidenceInfo(administratorID, residenceID) {
    this.residenceService
      .get(administratorID, residenceID)
      .valueChanges()
      .subscribe((data: Residence) => {
        this.residence = data;
      });
  }

  getResidentInfo(administratorID, residenceID, uid) {
    const residentQuery = this.residentService.getOne(administratorID, residenceID, uid);

    residentQuery.valueChanges().subscribe((data: Resident) => {
      this.resident = data;
    });
  }

  verificateUserProvider() {
    this.auth.isLogged().subscribe(result => {
      this.provider = result.providerData[0].providerId;
      if (result.providerData[0].providerId != 'password') {
        this.changePassword = false;
      }
    });
  }

  createSignupForm(): FormGroup {
    const minYear = parseInt(
      new Date(Date.now())
        .getFullYear()
        .toString()
        .substr(2, 2),
      0
    );
    return this.fb.group({
      number: [
        { value: '', disabled: true },
        Validators.compose([Validators.required, Validators.min(1000000000000000), Validators.max(9999999999999999)])
      ],
      name: [{ value: '', disabled: true }, Validators.compose([Validators.required])],
      month: [{ value: '', disabled: true }, Validators.compose([Validators.required, Validators.min(1), Validators.max(12)])],
      year: [
        { value: '', disabled: true },
        Validators.compose([Validators.required, Validators.min(minYear), Validators.max(minYear + 15)])
      ],
      cvv: [{ value: '', disabled: true }, Validators.compose([Validators.required, Validators.min(100), Validators.max(999)])]
    });
  }

  createAdministratorForm(): FormGroup {
    return this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(50)])],
      email: [null, Validators.compose([Validators.required, Validators.email])],
      code: [null, Validators.compose([Validators.required, Validators.max(999), Validators.pattern(/^-?(0|[1-9]\d*)?$/)])],
      number: [null, Validators.compose([Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)])]
    });
  }

  private getAdministratorInfo(administratorID) {
    this.administratorService
      .get(administratorID)
      .valueChanges()
      .subscribe((data: Administrator) => {
        this.administrator = data;
        this.getCardsInfo(administratorID);
      });
  }

  private getCardsInfo(administratorID) {
    this.cardService
      .get(administratorID)
      .get()
      .subscribe(result => {
        // console.log(result);
        if (!result.empty) {
          result.docs.forEach(info => {
            this.administrator.cc = { id: info.id, ...info.data() };
          });
          this.frmPlan.controls['number'].setValue(this.administrator.cc.number);
          this.frmPlan.controls['month'].setValue(this.administrator.cc.month);
          this.frmPlan.controls['year'].setValue(this.administrator.cc.year);
          this.frmPlan.controls['cvv'].setValue(this.administrator.cc.cvv);
          this.frmPlan.controls['name'].setValue(this.administrator.cc.name);
        } else {
          const card: CC = { id: '', alias: '', name: '', number: 0, month: 0, year: 0, cvv: 0 };
          this.administrator.cc = card;
        }
      });
  }

  resetPassword() {
    this.auth.sendEmail(
      this,
      this.resident.email,
      function s(context) {
        context.auth.logOut();
      },
      function e() {}
    );
  }

  saveCC(){
    
  }
}
