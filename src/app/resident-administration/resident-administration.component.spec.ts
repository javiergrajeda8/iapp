import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentAdministrationComponent } from './resident-administration.component';

describe('ResidentAdministrationComponent', () => {
  let component: ResidentAdministrationComponent;
  let fixture: ComponentFixture<ResidentAdministrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResidentAdministrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
