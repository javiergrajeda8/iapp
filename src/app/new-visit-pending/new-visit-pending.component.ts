import { Component, OnInit } from '@angular/core';
import { Residence } from '../interfaces/residence';
import { Resident } from '../interfaces/resident';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { VisitorService } from '../services/visitor.service';
import { Visitor } from '../interfaces/visitor';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-new-visit-pending',
  templateUrl: './new-visit-pending.component.html',
  styleUrls: ['./new-visit-pending.component.scss']
})
export class NewVisitPendingComponent implements OnInit {
  administratorID: any;
  residenceID: any;
  residence: Residence;
  residentID: any;
  resident: Resident;
  visitor: Visitor[];
  visitorID: any;
  visit: { name: ''; visitorID: '' };
  dateToday;
  constructor(
    private route: Router,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private visitorService: VisitorService
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    let dt = new Date(Date.now());
    dt = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
    this.dateToday = dt.getTime();
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.residentID = this.activatedRoute.snapshot.params[param3];
  }

  ngOnInit() {
    this.getPendingVisitor(this.administratorID, this.residenceID, this.residentID);
  }

  close() {
    this.modalService.dismissAll();
  }

  getPendingVisitor(administratorID, residenceID, residentID) {
    this.visitorService
      .get(administratorID, residenceID, residentID)
      .valueChanges()
      .subscribe((data: Visitor[]) => {
        this.visitor = data.filter(fn => fn.status === 1).sort((a, b) => (b.visitDate > a.visitDate ? 1 : -1));
      });
  }

  details(code, visitorID) {
    const queryParams: Params = { visitID: visitorID };

    this.route.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: queryParams,
      queryParamsHandling: 'merge' // remove to replace all query params by provided
    });
    this.modalService.open(code, { centered: true });
  }

  delete(content, visitorName, vis) {
    this.visit = { name: visitorName, visitorID: vis.toString() };
    // console.log(this.visit);
    this.modalService.open(content, { centered: true });
  }
  confirmDelete(administratorID, residenceID, residentID, visitorID) {
    // console.log(visitorID);
    this.visitorService.delete(administratorID, residenceID, residentID, visitorID).then(f => {
      this.modalService.dismissAll();
    });
  }
}
