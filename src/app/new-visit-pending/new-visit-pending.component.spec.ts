import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVisitPendingComponent } from './new-visit-pending.component';

describe('NewVisitPendingComponent', () => {
  let component: NewVisitPendingComponent;
  let fixture: ComponentFixture<NewVisitPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVisitPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVisitPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
