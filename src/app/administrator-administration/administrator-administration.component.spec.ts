import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministratorAdministrationComponent } from './administrator-administration.component';

describe('AdministratorAdministrationComponent', () => {
  let component: AdministratorAdministrationComponent;
  let fixture: ComponentFixture<AdministratorAdministrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministratorAdministrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministratorAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
