import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Administrator } from '../interfaces/administrator';
import { AuthorizationService } from '../services/authorization.service';
import { ActivatedRoute } from '@angular/router';
import { AdministratorService } from '../services/administrator.service';
import { AdministratorConfigurationService } from '../services/administrator-configuration.service';
import { CardService } from '../services/card.service';
import { ToastService } from '../services/toast.service';
import { min } from 'rxjs/operators';
import { CC } from '../interfaces/cc';

@Component({
  selector: 'app-administrator-administration',
  templateUrl: './administrator-administration.component.html',
  styleUrls: ['./administrator-administration.component.scss']
})
export class AdministratorAdministrationComponent implements OnInit {
  public frmPlan: FormGroup;
  public frmAdministration: FormGroup;
  administratorID;
  administrator: Administrator;
  changePassword = true;
  provider = '';

  constructor(
    private administratorService: AdministratorService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private auth: AuthorizationService,
    private cardService: CardService,
    private toastService: ToastService
  ) {
    const param1 = 'uid';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
  }

  ngOnInit() {
    this.frmPlan = this.createSignupForm();
    this.frmAdministration = this.createAdministratorForm();
    this.getAdministratorInfo(this.administratorID);
    this.setupCardValidations();
    this.verificateUserProvider();
  }

  verificateUserProvider() {
    this.auth.isLogged().subscribe(result => {
      this.provider = result.providerData[0].providerId;
      if (result.providerData[0].providerId != 'password') {
        this.changePassword = false;
      }
    });
  }

  setupCardValidations() {
    const minYear = parseInt(
      new Date(Date.now())
        .getFullYear()
        .toString()
        .substr(2, 2),
      0
    );
    this.frmPlan.controls['membership'].valueChanges.subscribe(checked => {
      if (checked != 'free') {
        this.frmPlan.controls['number'].setValidators([
          Validators.required,
          Validators.min(1000000000000000),
          Validators.max(9999999999999999)
        ]);
        this.frmPlan.controls['month'].setValidators([Validators.required, Validators.min(1), Validators.max(12)]);
        this.frmPlan.controls['year'].setValidators([Validators.required, Validators.min(minYear), Validators.max(minYear + 15)]);
        this.frmPlan.controls['cvv'].setValidators([Validators.required, Validators.min(100), Validators.max(999)]);
        this.frmPlan.controls['name'].setValidators([Validators.required]);
        this.frmPlan.controls['number'].enable();
        this.frmPlan.controls['month'].enable();
        this.frmPlan.controls['year'].enable();
        this.frmPlan.controls['cvv'].enable();
        this.frmPlan.controls['name'].enable();
      } else {
        this.frmPlan.controls['number'].setValidators(null);
        this.frmPlan.controls['month'].setValidators(null);
        this.frmPlan.controls['year'].setValidators(null);
        this.frmPlan.controls['cvv'].setValidators(null);
        this.frmPlan.controls['name'].setValidators(null);
        this.frmPlan.controls['number'].disable();
        this.frmPlan.controls['month'].disable();
        this.frmPlan.controls['year'].disable();
        this.frmPlan.controls['cvv'].disable();
        this.frmPlan.controls['name'].disable();
      }
      this.frmPlan.controls['number'].updateValueAndValidity();
      this.frmPlan.controls['month'].updateValueAndValidity();
      this.frmPlan.controls['year'].updateValueAndValidity();
      this.frmPlan.controls['cvv'].updateValueAndValidity();
      this.frmPlan.controls['name'].updateValueAndValidity();
    });
  }

  private getAdministratorInfo(administratorID) {
    this.administratorService
      .get(administratorID)
      .valueChanges()
      .subscribe((data: Administrator) => {
        this.administrator = data;
        this.frmPlan.controls['membership'].setValue(this.administrator.membership);
        this.frmAdministration.controls['name'].setValue(this.administrator.name);
        this.frmAdministration.controls['email'].setValue(this.administrator.email);
        this.frmAdministration.controls['code'].setValue(this.administrator.phoneCountryCode);
        this.frmAdministration.controls['number'].setValue(this.administrator.phone);
        this.getCardsInfo(administratorID);
      });
  }

  private getCardsInfo(administratorID) {
    this.cardService
      .get(administratorID)
      .get()
      .subscribe(result => {
        // console.log(result);
        if (!result.empty) {
          result.docs.forEach(info => {
            this.administrator.cc = { id: info.id, ...info.data() };
          });
          this.frmPlan.controls['number'].setValue(this.administrator.cc.number);
          this.frmPlan.controls['month'].setValue(this.administrator.cc.month);
          this.frmPlan.controls['year'].setValue(this.administrator.cc.year);
          this.frmPlan.controls['cvv'].setValue(this.administrator.cc.cvv);
          this.frmPlan.controls['name'].setValue(this.administrator.cc.name);
        } else {
          const card: CC = { id: '', alias: '', name: '', number: 0, month: 0, year: 0, cvv: 0 };
          this.administrator.cc = card;
        }
      });
  }

  createSignupForm(): FormGroup {
    const minYear = parseInt(
      new Date(Date.now())
        .getFullYear()
        .toString()
        .substr(2, 2),
      0
    );
    return this.fb.group({
      membership: [null, null],
      number: [null, Validators.compose([Validators.required, Validators.min(1000000000000000), Validators.max(9999999999999999)])],
      name: [null, Validators.compose([Validators.required])],
      month: [null, Validators.compose([Validators.required, Validators.min(1), Validators.max(12)])],
      year: [null, Validators.compose([Validators.required, Validators.min(minYear), Validators.max(minYear + 15)])],
      cvv: [null, Validators.compose([Validators.required, Validators.min(100), Validators.max(999)])]
    });
  }

  saveInformation() {
    this.administrator.name = this.frmAdministration.controls['name'].value;
    this.administrator.phone = this.frmAdministration.controls['number'].value;
    this.administrator.phoneCountryCode = this.frmAdministration.controls['code'].value;
    this.administratorService.set(this.administrator).then(result => {
      this.toastService.show('Information saved successfully', { classname: 'bg-success text-light p-2  my-5', delay: 5000 });
    });
  }

  saveCC() {
    this.administrator.membership = this.frmPlan.controls['membership'].value;
    this.administrator.cc.alias = 'Principal';
    this.administrator.cc.name = this.frmPlan.controls['name'].value;
    this.administrator.cc.cvv = this.frmPlan.controls['cvv'].value;
    this.administrator.cc.month = this.frmPlan.controls['month'].value;
    this.administrator.cc.year = this.frmPlan.controls['year'].value;
    this.administrator.cc.number = this.frmPlan.controls['number'].value;
    this.administratorService.update(this.administratorID, { membership: this.administrator.membership }).then(reuslt1 => {
      this.cardService.set(this.administratorID, this.administrator.cc).then(result => {
        this.toastService.show('Plan and payment information saved successfully', {
          classname: 'bg-success text-light p-2 my-5',
          delay: 5000
        });
      });
    });
  }

  createAdministratorForm(): FormGroup {
    return this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(50)])],
      email: [null, Validators.compose([Validators.required, Validators.email])],
      code: [null, Validators.compose([Validators.required, Validators.max(999), Validators.pattern(/^-?(0|[1-9]\d*)?$/)])],
      number: [null, Validators.compose([Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)])]
    });
  }

  resetPassword() {
    this.auth.sendEmail(
      this,
      this.administrator.email,
      function s(context) {
        context.auth.logOut();
      },
      function e() {}
    );
  }
}
