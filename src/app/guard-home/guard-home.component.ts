import { Component, OnInit } from '@angular/core';
import { Guard } from '../interfaces/guard';
import { Residence } from '../interfaces/residence';
import { Router, ActivatedRoute } from '@angular/router';
import { ResidenceService } from '../services/residence.service';

@Component({
  selector: 'app-guard-home',
  templateUrl: './guard-home.component.html',
  styleUrls: ['./guard-home.component.scss']
})
export class GuardHomeComponent implements OnInit {
  administratorID: any;
  residenceID: any;
  guard: Guard;
  residence: Residence;
  guardID: any;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private residenceService: ResidenceService) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.guardID = this.activatedRoute.snapshot.params[param3];
  }

  ngOnInit() {}
}
