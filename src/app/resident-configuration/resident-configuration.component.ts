import { Component, OnInit } from '@angular/core';
import { Resident } from '../interfaces/resident';
import { Residence } from '../interfaces/residence';
import { ResidentService } from '../services/resident.service';
import { ResidenceService } from '../services/residence.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Visitor } from '../interfaces/visitor';
import { Recurrent } from '../interfaces/recurrent';
import { AuthorizationService } from '../services/authorization.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Administrator } from '../interfaces/administrator';
import { AdministratorService } from '../services/administrator.service';
import { GuardService } from '../services/guard.service';
import { Guard } from '../interfaces/guard';
import { ToastService } from '../services/toast.service';
import { ResidentConfiguration } from '../interfaces/residentConfiguration';

@Component({
  selector: 'app-resident-configuration',
  templateUrl: './resident-configuration.component.html',
  styleUrls: ['./resident-configuration.component.scss']
})
export class ResidentConfigurationComponent implements OnInit {
  administratorID: any;
  administrator: Administrator;
  residenceID: any;
  resident: Resident;
  residence: Residence;
  uid: 0;
  frmVisitor: FormGroup;
  guards: Guard[];
  constructor(
    private administratorService: AdministratorService,
    private auth: AuthorizationService,
    private residentService: ResidentService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private residenceService: ResidenceService,
    private guardsService: GuardService,
    private fb: FormBuilder,
    private toastService: ToastService
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.uid = this.activatedRoute.snapshot.params[param3];
  }

  ngOnInit() {
    this.frmVisitor = this.createVisitorForm();
    this.getResidenceInfo(this.administratorID, this.residenceID);
    this.getAdministratorInfo(this.administratorID);
    this.getGuardsInformation(this.administratorID, this.residenceID);
  }

  private getGuardsInformation(administratorID, residenceID) {
    this.guardsService
      .get(administratorID, residenceID)
      .valueChanges()
      .subscribe((data: Guard[]) => {
        this.guards = data;
      });
  }

  private getAdministratorInfo(administratorID) {
    this.administratorService
      .get(administratorID)
      .valueChanges()
      .subscribe((data: Administrator) => {
        this.administrator = data;
      });
  }

  createVisitorForm(): FormGroup {
    return this.fb.group({
      SMS: [null, null],
      email: [null, null],
      recurrent: [null, null],
      notificateMe: [null, null]
    });
  }

  getResidenceInfo(administratorID, residenceID) {
    // console.log(administratorID, residenceID);
    this.residenceService
      .get(administratorID, residenceID)
      .valueChanges()
      .subscribe((data: Residence) => {
        this.residence = data;
        this.getResidentInfo(this.administratorID, this.residenceID, this.uid);
      });
  }

  saveRegisterConfiguration() {
    // console.log('save');
    const visitorConfig: ResidentConfiguration = {
      visitor: {
        SMS: this.frmVisitor.controls['SMS'].value,
        email: this.frmVisitor.controls['email'].value,
        recurrent: this.frmVisitor.controls['recurrent'].value,
        notificateMe: this.frmVisitor.controls['notificateMe'].value
      }
    };
    this.resident.configuration = visitorConfig;
    this.residentService.set(this.administratorID, this.residenceID, this.resident).then(result => {
      this.toastService.show('Configuration saved successfully', { classname: 'bg-success text-light my-5', delay: 5000 });
    });
  }

  getResidentInfo(administratorID, residenceID, uid) {
    const residentQuery = this.residentService.getOne(administratorID, residenceID, uid);

    residentQuery.valueChanges().subscribe((data: Resident) => {
      this.resident = data;
      this.frmVisitor.controls['SMS'].setValue(this.resident.configuration.visitor.SMS);
      this.frmVisitor.controls['recurrent'].setValue(this.resident.configuration.visitor.recurrent);
      this.frmVisitor.controls['notificateMe'].setValue(this.resident.configuration.visitor.notificateMe);
      this.frmVisitor.controls['email'].setValue(this.resident.configuration.visitor.email);
      // this.auth.getUID().then(result => {
      //   if (result.uid === this.resident.uid || this.uid != 0) {
      //     if (result.uid === this.resident.uid) {
      //       if (this.resident.status === 0) {
      //         this.router.navigate(['residentForm/' + this.administratorID + '/' + this.residenceID + '/' + this.uid]);
      //       } else {
      //         residentQuery
      //           .collection('visitor')
      //           .valueChanges()
      //           .subscribe((data2: Visitor[]) => {
      //             this.resident.Visitor = data2.filter(fn => fn.status === 1);
      //             residentQuery
      //               .collection('recurrent')
      //               .valueChanges()
      //               .subscribe((data3: Recurrent[]) => {
      //                 this.resident.Recurrent = data3.filter(fn => fn.status === 1);
      //                 // console.log(this.resident);
      //               });
      //           });
      //       }
      //     }
      //   }
      //   // console.log('LOGGED UID', result.uid);
      //   // console.log('UID TO EDIT', this.resident.uid);
      // });
    });
  }
}
