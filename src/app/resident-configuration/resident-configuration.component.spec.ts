import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentConfigurationComponent } from './resident-configuration.component';

describe('ResidentConfigurationComponent', () => {
  let component: ResidentConfigurationComponent;
  let fixture: ComponentFixture<ResidentConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResidentConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
