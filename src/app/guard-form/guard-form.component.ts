import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from '../services/custom-validators';
import { ActivatedRoute, Router } from '@angular/router';
import { GuardService } from '../services/guard.service';
import { Guard } from '../interfaces/guard';

@Component({
  selector: 'app-guard-form',
  templateUrl: './guard-form.component.html',
  styleUrls: ['./guard-form.component.scss']
})
export class GuardFormComponent implements OnInit {
  residenceID: any;
  administratorID: any;
  frmGuard: FormGroup;
  guard: Guard;
  uid: 0;
  constructor(private fb: FormBuilder, private activatedRoute: ActivatedRoute, private router: Router, private guardService: GuardService) {
    this.administratorID = this.activatedRoute.snapshot.params['uid'];
    this.residenceID = this.activatedRoute.snapshot.params['uid2'];
    this.uid = this.activatedRoute.snapshot.params['uid3'];
  }

  ngOnInit() {
    this.frmGuard = this.createGuardForm();
    if (this.uid != 0) {
      this.getGuardInfo(this.administratorID, this.residenceID, this.uid);
    }
  }

  getGuardInfo(administratorID, residenceID, uid) {
    this.guardService
      .getOne(administratorID, residenceID, uid)
      .valueChanges()
      .subscribe((data: Guard) => {
        this.guard = data;
        this.frmGuard.controls['userName'].setValue(this.guard.email);
        this.frmGuard.controls['name'].setValue(this.guard.name);
        this.frmGuard.controls['password'].setValue(this.guard.password);
        this.frmGuard.controls['code'].setValue(this.guard.phoneCountryCode);
        this.frmGuard.controls['number'].setValue(this.guard.phone);
        // this.residenceTypeService = result;
        // result.docs.forEach(i => console.log(i.get()));
      });
  }

  createGuardForm(): FormGroup {
    return this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(50)])],
      code: [null, Validators.compose([Validators.required, Validators.max(999), Validators.pattern(/^-?(0|[1-9]\d*)?$/)])],
      number: [null, Validators.compose([Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)])],
      userName: [null, Validators.compose([Validators.required, Validators.maxLength(30)])],
      password: [
        null,
        Validators.compose([
          Validators.required,
          Validators.min(100000),
          Validators.max(9999999999),
          Validators.pattern(/^-?(0|[1-9]\d*)?$/)
        ])
      ]
    });
  }

  save() {
    const guard: Guard = {
      uid: this.frmGuard.controls['userName'].value.toUpperCase(),
      email: this.frmGuard.controls['userName'].value.toUpperCase(),
      name: this.frmGuard.controls['name'].value,
      password: this.frmGuard.controls['password'].value,
      phoneCountryCode: this.frmGuard.controls['code'].value,
      phone: this.frmGuard.controls['number'].value,
      status: 1
    };
    this.guardService.new(this.administratorID, this.residenceID, guard, callback => {
      // this.frmGuard.controls['userName'].setValue('');
      // this.frmGuard.controls['name'].setValue('');
      // this.frmGuard.controls['password'].setValue('');
      // this.frmGuard.controls['code'].setValue('');
      // this.frmGuard.controls['number'].setValue('');
      this.router.navigate(['guard/' + this.administratorID + '/' + this.residenceID]);
    });
  }

  generateLoginToken() {
    this.frmGuard.controls['password'].setValue((Math.random() * 1000000).toFixed(0));
  }

  generateUser() {
    let RESIDENCEID = this.residenceID;
    try {
      // tslint:disable-next-line: radix
      while (!isNaN(parseInt(RESIDENCEID.substring(RESIDENCEID.length - 1, RESIDENCEID.length)))) {
        RESIDENCEID = this.residenceID.substring(0, RESIDENCEID.length - 1);
      }
    } catch (error) {}
    RESIDENCEID += (Math.random() * 1000).toFixed(0);
    this.frmGuard.controls['userName'].setValue(RESIDENCEID);
  }
}
