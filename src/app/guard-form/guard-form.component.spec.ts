import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuardFormComponent } from './guard-form.component';

describe('GuardFormComponent', () => {
  let component: GuardFormComponent;
  let fixture: ComponentFixture<GuardFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuardFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
