import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../services/authorization.service';
import { Router } from '@angular/router';
import { QuerySnapshot } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  msj = '';
  public frmSignup: FormGroup;
  constructor(private auth: AuthorizationService, private router: Router, private fb: FormBuilder) {}

  ngOnInit() {
    this.auth.getUID().then(result => {
      if (result) {
        this.auth.redirectUser(this.auth, this.router, result.email);
      }
    });
    this.frmSignup = this.createSignupForm();
  }

  createSignupForm(): FormGroup {
    return this.fb.group({
      email: [null, Validators.compose([Validators.email, Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });
  }

  authenticate() {
    this.auth.login(
      this.frmSignup.controls['email'].value,
      this.frmSignup.controls['password'].value,
      this,
      function success(context, usr) {
        context.auth.redirectUser(context.auth, context.router, usr.email);
      },
      function error(context, err) {
        // console.log(err);
        if (err.code == 'auth/wrong-password') {
          context.msj = 'User / Password incorrect';
        }
      }
    );
  }

  loginWithFacebook() {
    this.auth.loginWithFacebook(
      this,
      function success(context, usr) {
        context.auth.redirectUser(context.auth, context.router, usr.email);
      },
      function error(context, err) {
        console.log(err);
      }
    );
  }

  loginWithGoogle() {
    this.auth.loginWithGoogle(
      this,
      function success(context, usr) {
        console.log(usr);
        context.auth.redirectUser(context.auth, context.router, usr.email);
      },
      function error(context, err) {
        console.log(err);
      }
    );
  }
}
