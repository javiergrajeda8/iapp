import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resident-reports',
  templateUrl: './resident-reports.component.html',
  styleUrls: ['./resident-reports.component.scss']
})
export class ResidentReportsComponent implements OnInit {
  administratorID;
  residenceID;
  residentID;
  constructor(private activatedRoute: ActivatedRoute) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.residentID = this.activatedRoute.snapshot.params[param3];
  }

  ngOnInit() {}
}
