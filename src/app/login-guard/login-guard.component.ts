import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AuthorizationService } from '../services/authorization.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-guard',
  templateUrl: './login-guard.component.html',
  styleUrls: ['./login-guard.component.scss']
})
export class LoginGuardComponent implements OnInit {
  msj = '';
  public frmSignup: FormGroup;
  constructor(private auth: AuthorizationService, private router: Router, private fb: FormBuilder) {}

  ngOnInit() {
    // this.auth.getUID().then(result => {
    //   if (result) {
    //     this.auth.redirectUser(this.auth, this.router, result.email);
    //   }
    // });
    this.frmSignup = this.createSignupForm();
  }

  createSignupForm(): FormGroup {
    return this.fb.group({
      email: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });
  }

  authenticateGuard() {
    this.auth.loginGuard(
      this,
      this.frmSignup.controls['email'].value,
      this.frmSignup.controls['password'].value,
      function success(context, existe, administratorID, residenceID, guardID) {
        if (existe) {
          context.router.navigate(['guardHome/' + administratorID + '/' + residenceID + '/' + guardID]);
        } else {
          context.msj = 'User / Password incorrect';
        }
      },
      function error(context, err) {
        if (err.code == 'auth/wrong-password') {
          context.msj = 'User / Password incorrect';
        }
      }
    );
  }
}
