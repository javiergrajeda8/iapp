import { Component, OnInit, ɵSWITCH_VIEW_CONTAINER_REF_FACTORY__POST_R3__ } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal, NgbCalendar, NgbPeriod } from '@ng-bootstrap/ng-bootstrap';
import { MiscellaneousService } from '../services/miscellaneous.service';
import { map } from 'rxjs/operators';
import { Code } from '../interfaces/code';
import { FirebaseStorage } from '@angular/fire';
import { Visitor } from '../interfaces/visitor';
import { ActivatedRoute, Router } from '@angular/router';
import { VisitorService } from '../services/visitor.service';
import { AuthorizationService } from '../services/authorization.service';
import { RecurrentService } from '../services/recurrent.service';
import { Recurrent } from '../interfaces/recurrent';
import { CodeService } from '../services/code.service';
import { Resident } from '../interfaces/resident';
import { ResidentService } from '../services/resident.service';
import { AdministratorService } from '../services/administrator.service';
import { Administrator } from '../interfaces/administrator';
import { ResidenceService } from '../services/residence.service';
import { Residence } from '../interfaces/residence';

@Component({
  selector: 'app-new-visit',
  templateUrl: './new-visit.component.html',
  styleUrls: ['./new-visit.component.scss']
})
export class NewVisitComponent implements OnInit {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  frmVisitor: FormGroup;
  code = 'Code';
  residenceID: any;
  administratorID: any;
  uid: any;
  recurrentID: any;
  recurrent: Recurrent;
  resident: Resident;
  residence: Residence;
  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private visitorService: VisitorService,
    private misce: MiscellaneousService,
    private calendar: NgbCalendar,
    private router: Router,
    private recurrentService: RecurrentService,
    private codeService: CodeService,
    private residentService: ResidentService,
    private residenceService: ResidenceService
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    const param4 = 'uid4';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.uid = this.activatedRoute.snapshot.params[param3];
    this.recurrentID = this.activatedRoute.snapshot.params[param4];
  }

  ngOnInit() {
    this.frmVisitor = this.createVisitorForm();
    this.getResidentInfo(this.administratorID, this.residenceID, this.uid);
    this.getResidenceInfo(this.administratorID, this.residenceID);
    this.frmVisitor.controls['visitDate'].setValue(this.calendar.getToday());

    this.gc();
    if (this.recurrentID !== '0') {
      this.getRecurrentInfo(this.administratorID, this.residenceID, this.uid, this.recurrentID);
    }
  }

  private getResidenceInfo(administratorID, residenceID) {
    this.residenceService
      .get(administratorID, residenceID)
      .valueChanges()
      .subscribe((data: Residence) => {
        // console.log(data);
        this.residence = data;
        this.verificateValidators(this.residence, this.frmVisitor);
      });
  }

  private verificateValidators(residence: Residence, frm) {
    if (residence.configuration.visitor.email) {
      frm.controls['email'].setValidators(Validators.compose([Validators.required, Validators.email]));
    } else {
      frm.controls['email'].setValidators(null);
    }
    frm.controls['email'].updateValueAndValidity();
    if (residence.configuration.visitor.badgeNumber) {
      frm.controls['badge'].setValidators(Validators.compose([Validators.required]));
    } else {
      frm.controls['badge'].setValidators(null);
    }
    frm.controls['badge'].updateValueAndValidity();
    if (residence.configuration.visitor.documentImage) {
      frm.controls['documentImage'].setValidators(Validators.compose([Validators.required]));
    } else {
      frm.controls['documentImage'].setValidators(null);
    }
    frm.controls['documentImage'].updateValueAndValidity();
    if (residence.configuration.visitor.identificationDocument) {
      frm.controls['documentNumber'].setValidators(Validators.compose([Validators.required, Validators.email]));
    } else {
      frm.controls['documentNumber'].setValidators(null);
    }
    frm.controls['documentNumber'].updateValueAndValidity();
    if (residence.configuration.visitor.phone) {
      frm.controls['code'].setValidators(
        Validators.compose([Validators.required, Validators.max(999), Validators.pattern(/^-?(0|[1-9]\d*)?$/)])
      );
      frm.controls['number'].setValidators(Validators.compose([Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]));
    } else {
      frm.controls['code'].setValidators(null);
      frm.controls['number'].setValidators(null);
    }
    frm.controls['code'].updateValueAndValidity();
    frm.controls['number'].updateValueAndValidity();
  }

  getResidentInfo(administratorID, residenceID, uid) {
    const residentQuery = this.residentService.getOne(administratorID, residenceID, uid);
    residentQuery.valueChanges().subscribe((data: Resident) => {
      this.resident = data;
      // console.log(this.resident.configuration);
      this.frmVisitor.controls['sendSMS'].setValue(this.resident.configuration.visitor.SMS);
      this.frmVisitor.controls['saveRecurrent'].setValue(this.resident.configuration.visitor.recurrent);
      this.frmVisitor.controls['notificateMe'].setValue(this.resident.configuration.visitor.notificateMe);
      this.frmVisitor.controls['sendEmail'].setValue(this.resident.configuration.visitor.email);
    });
  }

  getRecurrentInfo(administratorID, residenceID, residentID, recurrentID) {
    console.log(administratorID, residentID, residenceID, recurrentID);
    this.recurrentService
      .getOne(administratorID, residenceID, residentID, recurrentID)
      .valueChanges()
      .subscribe((data: Recurrent) => {
        this.recurrent = data;
        this.frmVisitor.controls['documentType'].setValue(this.recurrent.documentType);
        this.frmVisitor.controls['documentImage'].setValue(this.recurrent.documentImage);
        this.frmVisitor.controls['documentNumber'].setValue(this.recurrent.documentNumber);
        this.frmVisitor.controls['vehiculeType'].setValue(this.recurrent.vehiculeType);
        this.frmVisitor.controls['badge'].setValue(this.recurrent.badge);
        this.frmVisitor.controls['name'].setValue(this.recurrent.name);
        this.frmVisitor.controls['email'].setValue(this.recurrent.eMail);
        this.frmVisitor.controls['number'].setValue(this.recurrent.phone);
        this.frmVisitor.controls['code'].setValue(this.recurrent.phoneCountryCode);
        this.frmVisitor.controls['sendEmail'].setValue(this.recurrent.notificateHimEmail);
        this.frmVisitor.controls['sendSMS'].setValue(this.recurrent.notificateHimSMS);
        this.frmVisitor.controls['notificateMe'].setValue(this.recurrent.notificateMe);
        this.frmVisitor.controls['saveRecurrent'].setValue(true);
        this.visitorService.getDocument('documents', this.recurrent.documentImage).subscribe(p => {
          this.croppedImage = p;
        });
      });
  }

  gc() {
    const rnd = (Math.random() * 1000).toFixed(0);
    this.misce.getCode().then((data: Code) => {
      this.code = data.value + rnd.toString();
    });
  }

  createVisitorForm(): FormGroup {
    return this.fb.group({
      documentType: ['DPI', Validators.compose([Validators.required])],
      documentNumber: [null, Validators.compose([Validators.required])],
      vehiculeType: ['Car', Validators.compose([Validators.required])],
      badge: [null, null],
      name: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(50)])],
      email: [null, Validators.compose([Validators.required, Validators.email])],
      code: [null, Validators.compose([Validators.required, Validators.max(999), Validators.pattern(/^-?(0|[1-9]\d*)?$/)])],
      number: [null, Validators.compose([Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)])],
      sendEmail: [false, null],
      sendSMS: [false, null],
      saveRecurrent: [false, null],
      notificateMe: [false, null],
      documentImage: [null, Validators.compose([Validators.required])],
      visitDate: [this.calendar.getToday(), Validators.compose([Validators.required])]
    });
  }

  selectToday(dayAdd) {
    console.log(this.frmVisitor.controls['visitDate'].value);
    if (dayAdd === 0) {
      this.frmVisitor.controls['visitDate'].setValue(this.calendar.getToday());
    } else if (dayAdd === 1) {
      this.frmVisitor.controls['visitDate'].setValue(this.calendar.getNext(this.calendar.getToday()));
    } else if (dayAdd === 2) {
      this.frmVisitor.controls['visitDate'].setValue(this.calendar.getNext(this.calendar.getNext(this.calendar.getToday())));
    }
  }

  cancel() {
    this.router.navigate(['residentHome/' + this.administratorID + '/' + this.residenceID + '/' + this.uid]);
  }

  save() {
    const visitor: Visitor = {
      documentType: this.frmVisitor.controls['documentType'].value,
      documentNumber: this.frmVisitor.controls['documentNumber'].value,
      documentImage: Date.now(),
      vehiculeType: this.frmVisitor.controls['vehiculeType'].value,
      badge: this.frmVisitor.controls['badge'].value,
      name: this.frmVisitor.controls['name'].value,
      visitDate: new Date(
        this.frmVisitor.controls['visitDate'].value.year,
        this.frmVisitor.controls['visitDate'].value.month - 1,
        this.frmVisitor.controls['visitDate'].value.day
      ).getTime(),
      eMail: this.frmVisitor.controls['email'].value,
      phone: this.frmVisitor.controls['number'].value,
      phoneCountryCode: this.frmVisitor.controls['code'].value,
      status: 1,
      notificateHimEmail: this.frmVisitor.controls['sendEmail'].value,
      notificateHimSMS: this.frmVisitor.controls['sendSMS'].value,
      notificateMe: this.frmVisitor.controls['notificateMe'].value,
      recurrent: this.frmVisitor.controls['saveRecurrent'].value,
      code: this.code
    };
    const pictures = this.visitorService.saveDocument(visitor.documentImage, this.croppedImage, 'data_url');
    pictures
      .then(result => {
        // console.log('storage', result);
        this.visitorService.set(this, this.administratorID, this.residenceID, this.uid, visitor, function cllbck(context, vs) {
          if (vs.recurrent) {
            context.recurrentService.set(context, context.administratorID, context.residenceID, context.uid, vs, function cllbck2(
              context2,
              vs2
            ) {
              const cod: Code = {
                value: vs2.code,
                residenceID: context2.residenceID,
                residentID: context2.uid,
                visitorID: vs2.documentImage,
                status: 1,
                visitDate: vs2.visitDate,
                name: vs2.name
              };
              context2.codeService.set(context2.administratorID, context2.residenceID, cod, function n() {});
              context2.router.navigate([
                'visitorCode/' + context2.administratorID + '/' + context2.residenceID + '/' + context2.uid + '/' + vs2.documentImage
              ]);
            });
          } else {
            context.router.navigate([
              'visitorCode/' + context.administratorID + '/' + context.residenceID + '/' + context.uid + '/' + vs.documentImage
            ]);
          }
        });
      })
      .catch(error => console.log(error));
    // console.log(visitor);
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  uploadImage(content) {
    this.modalService.open(content, { centered: true });
  }
}
