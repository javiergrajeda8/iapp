import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportVisitWeekComponent } from './report-visit-week.component';

describe('ReportVisitWeekComponent', () => {
  let component: ReportVisitWeekComponent;
  let fixture: ComponentFixture<ReportVisitWeekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportVisitWeekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportVisitWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
