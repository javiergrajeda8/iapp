import { Component, OnInit } from '@angular/core';
import { GuardService } from '../services/guard.service';
import { ActivatedRoute } from '@angular/router';
import { Code } from '../interfaces/code';
import { ResidenceService } from '../services/residence.service';
import { ResidentService } from '../services/resident.service';
import { Residence } from '../interfaces/residence';
import { Resident } from '../interfaces/resident';
@Component({
  selector: 'app-report-visit-week',
  templateUrl: './report-visit-week.component.html',
  styleUrls: ['./report-visit-week.component.scss']
})
export class ReportVisitWeekComponent implements OnInit {
  administratorID: any;
  residenceID: any;
  guardID: any;
  codes: Code[] = [];
  code: Code;
  dateToday: any;
  constructor(
    private residentService: ResidentService,
    private residenceService: ResidenceService,
    private guardService: GuardService,
    private activatedRoute: ActivatedRoute
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.guardID = this.activatedRoute.snapshot.params[param3];
  }

  ngOnInit() {
    const now = Date.now();
    let dateTo = new Date(now);
    dateTo = new Date(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate());
    this.dateToday = dateTo.getTime();
    // let dateFrom = new Date(dateToday.getTime() - 86400);
    let report = this.guardService.getAccessCodeLastWeek(
      this.administratorID,
      this.residenceID,
      dateTo.getTime() + (86400000 * 3),
      dateTo.getTime() - (86400000 * 7)
    );
    report.then(result => {
      result.docs.forEach(r => {
        this.codes.push({ ...r.data() });
      });
      this.codes.forEach(i => {
        this.residenceService
          .get(this.administratorID, this.residenceID)
          .valueChanges()
          .subscribe((data: Residence[]) => (i.Residence = data));
        this.residentService
          .getOne(this.administratorID, this.residenceID, i.residentID)
          .valueChanges()
          .subscribe((data: Resident[]) => {
            i.Resident = data;
          });
      });
      // console.log(this.codes);
    });
  }
}
