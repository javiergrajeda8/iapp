import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVisitCodeComponent } from './new-visit-code.component';

describe('NewVisitCodeComponent', () => {
  let component: NewVisitCodeComponent;
  let fixture: ComponentFixture<NewVisitCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVisitCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVisitCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
