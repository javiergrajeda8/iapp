import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ResidentService } from '../services/resident.service';
import { ResidenceService } from '../services/residence.service';
import { Residence } from '../interfaces/residence';
import { Resident } from '../interfaces/resident';
import { VisitorService } from '../services/visitor.service';
import { Visitor } from '../interfaces/visitor';

@Component({
  selector: 'app-new-visit-code',
  templateUrl: './new-visit-code.component.html',
  styleUrls: ['./new-visit-code.component.scss']
})
export class NewVisitCodeComponent implements OnInit {
  residenceID: any;
  residence: Residence;
  administratorID: any;
  residentID: any;
  resident: Resident;
  visitID: any;
  visitor: Visitor;
  constructor(
    private activatedRoute: ActivatedRoute,
    private residentService: ResidentService,
    private residenceService: ResidenceService,
    private visitorService: VisitorService
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    const param4 = 'uid4';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.residentID = this.activatedRoute.snapshot.params[param3];
    this.visitID = this.activatedRoute.snapshot.params[param4];
    if (!this.visitID) {
      this.activatedRoute.queryParams.subscribe(f => {
        this.visitID = f.visitID;
      });
    }
  }

  ngOnInit() {
    this.getResidentInfo(this.administratorID, this.residenceID, this.residentID);
    this.getResidenceInfo(this.administratorID, this.residenceID);
    this.getVisitorInfo(this.administratorID, this.residenceID, this.residentID, this.visitID);
  }

  getResidenceInfo(administratorID, residenceID) {
    this.residenceService
      .get(administratorID, residenceID)
      .valueChanges()
      .subscribe((data: Residence) => {
        this.residence = data;
      });
  }

  getResidentInfo(administratorID, residenceID, uid) {
    this.residentService
      .getOne(administratorID, residenceID, uid)
      .valueChanges()
      .subscribe((data: Resident) => {
        this.resident = data;
      });
  }

  getVisitorInfo(administratorID, residenceID, residentID, visitID) {
    this.visitorService
      .getOne(administratorID, residenceID, residentID, visitID)
      .valueChanges()
      .subscribe((data: Visitor) => {
        this.visitor = data;
        this.getImage(this.visitor.documentImage);
      });
  }

  getImage(visitID) {
    this.visitorService.getDocument('documents', visitID).subscribe(p => {
      this.visitor.documentImage = p;
    });
  }
}
