import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministratorReportsComponent } from './administrator-reports.component';

describe('AdministratorReportsComponent', () => {
  let component: AdministratorReportsComponent;
  let fixture: ComponentFixture<AdministratorReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministratorReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministratorReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
