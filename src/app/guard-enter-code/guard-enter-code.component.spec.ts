import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuardEnterCodeComponent } from './guard-enter-code.component';

describe('GuardEnterCodeComponent', () => {
  let component: GuardEnterCodeComponent;
  let fixture: ComponentFixture<GuardEnterCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardEnterCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuardEnterCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
