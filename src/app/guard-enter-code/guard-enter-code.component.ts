import { Component, OnInit } from '@angular/core';
import { Residence } from '../interfaces/residence';
import { ResidenceService } from '../services/residence.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GuardService } from '../services/guard.service';

@Component({
  selector: 'app-guard-enter-code',
  templateUrl: './guard-enter-code.component.html',
  styleUrls: ['./guard-enter-code.component.scss']
})
export class GuardEnterCodeComponent implements OnInit {
  residenceID: any;
  guardID: any;
  administratorID: any;
  residence: Residence;
  frmCode: FormGroup;
  valido: boolean;
  evaluado = false;
  constructor(
    private guardService: GuardService,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private residenceService: ResidenceService,
    private route: Router
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.guardID = this.activatedRoute.snapshot.params[param3];
  }

  ngOnInit() {
    this.frmCode = this.createCodeForm();
    this.getResidenceInfo(this.administratorID, this.residenceID);
  }

  tryAccessCode() {
    const code = this.guardService
      .searchAccessCode(this.administratorID, this.residenceID, this.frmCode.controls['visitCode'].value)
      .snapshotChanges()
      .subscribe(f => {
        this.evaluado = true;
        // console.log(f);
        if (f.length > 0) {
          // console.log(f[0].payload.doc.data());
          this.valido = true;
          const now = Date.now();
          let dateToday = new Date(now);
          dateToday = new Date(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());
          this.guardService
            .validAccessCode(
              this.administratorID,
              this.residenceID,
              f[0].payload.doc.data().residentID,
              f[0].payload.doc.data().visitorID.toString(),
              this.frmCode.controls['visitCode'].value,
              dateToday.getTime()
            )
            .snapshotChanges()
            .subscribe(r => {
              const me = this;
              setTimeout(function() {
                const queryParams: Params = { residentID: f[0].payload.doc.data().residentID, visitID: f[0].payload.doc.data().visitorID };
                me.route.navigate(['codeCard/' + me.administratorID + '/' + me.residenceID + '/' + me.guardID], {
                  queryParams,
                  queryParamsHandling: 'merge' // remove to replace all query params by provided
                });
                // console.log(r);
              }, 1000);
            });
          const queryParams: Params = { residentID: f[0].payload.doc.data().residentID, visitID: f[0].payload.doc.data().visitorID };
          this.route.navigate([], {
            relativeTo: this.activatedRoute,
            queryParams,
            queryParamsHandling: 'merge' // remove to replace all query params by provided
          });
        } else {
          this.valido = false;
        }

        // if (f.payload.exists) {
        //   alert('Codigo válido');
        // } else {
        //   alert('Codigo inválido');
        // }
      });
  }

  createCodeForm(): FormGroup {
    return this.fb.group({
      visitCode: [null, Validators.compose([Validators.required])]
    });
  }

  getResidenceInfo(administratorID, residenceID) {
    this.residenceService
      .get(administratorID, residenceID)
      .valueChanges()
      .subscribe((data: Residence) => {
        this.residence = data;
      });
  }
}
