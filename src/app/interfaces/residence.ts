import { ResidenceType } from './residenceType';
import { Resident } from './resident';
import { Code } from './code';
import { AdministratorConfiguration } from './administratorConfiguration';

export interface Residence {
    name: string;
    code: string;
    type: number;
    membership: number;
    status: number;
    address?: string;
    resident?: Resident[];
    codes?: Code[];
    configuration?: AdministratorConfiguration;

}
