export interface ResidentConfiguration {
  visitor: {
    SMS?: boolean;
    email?: boolean;
    recurrent?: boolean;
    notificateMe?: boolean;
  };
}
