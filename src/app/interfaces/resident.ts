import { User } from './user';
import { Recurrent } from './recurrent';
import { Visitor } from './visitor';
import { ResidentConfiguration } from './residentConfiguration';

export interface Resident extends User {
    
    residenceCode?: string;
    street?: string;
    residenceNumber?: number;
    level?: number;
    residenceType?: any;
    residentType?: any;
    streetType?: any;
    photo?: any;
    password?: string;
    status: number;
    comments?: string;
    Recurrent?: Recurrent[];
    Visitor?: Visitor[];
    image?: any;
    configuration?: ResidentConfiguration;
}
