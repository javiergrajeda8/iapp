import { User } from './user';

export interface Guard extends User {
  password: string;
  status: number;
}
