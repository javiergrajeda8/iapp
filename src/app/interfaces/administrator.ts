import { User } from './user';
import { Residence } from './residence';
import { CC } from './cc';

export interface Administrator extends User {
  residence?: Residence[];
  status: number;
  membership: number;
  cc?: CC;
}
