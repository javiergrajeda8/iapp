import { Resident } from './resident';

export interface Recurrent {
  documentType: string;
  documentNumber: string;
  documentImage: any;
  vehiculeType: any;
  badge?: string;
  name: string;
  visitDate: number;
  eMail?: string;
  phone?: number;
  phoneCountryCode?: number;
  status: number;
  notificateHimEmail: boolean;
  notificateHimSMS: boolean;
  notificateMe: boolean;
  Resident?: Resident;
  recurrent: boolean;
  code: string;
  rejectReason?: string;
  rejectObservation?: string;
}
