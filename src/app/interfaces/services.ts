import { AdministratorService } from '../services/administrator.service';
import { ResidenceService } from '../services/residence.service';
import { GuardService } from '../services/guard.service';

export interface Services {
    administratorService?: AdministratorService;
    residenceService?: ResidenceService;
    guardService?: GuardService;
    residentService?: ResidenceService;
}
