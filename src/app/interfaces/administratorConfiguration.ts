export interface AdministratorConfiguration {
  visitor: {
    documentImage: boolean;
    identificationDocument: boolean;
    badgeNumber: boolean;
    email: boolean;
    phone: boolean;
  };
  register: {
    residenceImage: boolean;
    residenceNumber: boolean;
    streetNumber: boolean;
    phone: boolean;
    listStreet: {
      active: boolean;
      streetNumber?: number[];
      avenueNumber?: number[];
    };
  };
}
