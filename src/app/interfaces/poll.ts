export interface Poll {
  role: string;
  option: string;
  like: boolean;
  reference?: string;
}
