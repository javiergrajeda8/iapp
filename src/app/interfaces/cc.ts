export interface CC {
  alias?: string;
  name?: string;
  number?: number;
  month?: number;
  year?: number;
  cvv?: number;
  id?: string;
}
