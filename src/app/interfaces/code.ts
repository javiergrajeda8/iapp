import { Resident } from './resident';
import { Visitor } from '@angular/compiler';
import { Residence } from './residence';
import { Guard } from './guard';

export interface Code {
  value?: string;
  residenceID?: string;
  residentID?: string;
  visitorID?: string;
  status?: number;
  visitor?: Visitor;
  name?: string;
  visitDate?: number;
  Residence?: Residence[];
  Resident?: Resident[];
  rejectReason?: string;
  rejectObservation?: string;
  confirmVisit?: number;
  Guard?: Guard;
}
