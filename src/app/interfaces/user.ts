export interface User {
  uid: string;
  email: string;
  name?: string;
  phone?: number;
  phoneCountryCode?: number;
}
