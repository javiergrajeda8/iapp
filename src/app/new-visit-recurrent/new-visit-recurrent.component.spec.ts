import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVisitRecurrentComponent } from './new-visit-recurrent.component';

describe('NewVisitRecurrentComponent', () => {
  let component: NewVisitRecurrentComponent;
  let fixture: ComponentFixture<NewVisitRecurrentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVisitRecurrentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVisitRecurrentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
