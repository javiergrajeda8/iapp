import { Component, OnInit } from '@angular/core';
import { Residence } from '../interfaces/residence';
import { Resident } from '../interfaces/resident';
import { ActivatedRoute } from '@angular/router';
import { RecurrentService } from '../services/recurrent.service';
import { Recurrent } from '../interfaces/recurrent';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-new-visit-recurrent',
  templateUrl: './new-visit-recurrent.component.html',
  styleUrls: ['./new-visit-recurrent.component.scss']
})
export class NewVisitRecurrentComponent implements OnInit {
  administratorID: any;
  residenceID: any;
  residence: Residence;
  residentID: any;
  resident: Resident;
  recurrent: Recurrent[];
  recurr: { name: ''; eMail: '' };
  constructor(private activatedRoute: ActivatedRoute, private modalService: NgbModal, private recurrentService: RecurrentService) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.residentID = this.activatedRoute.snapshot.params[param3];
  }

  ngOnInit() {
    this.getRecurrentVisitor(this.administratorID, this.residenceID, this.residentID);
  }

  getRecurrentVisitor(administratorID, residenceID, residentID) {
    this.recurrentService
      .get(administratorID, residenceID, residentID)
      .valueChanges()
      .subscribe((data: Recurrent[]) => {
        this.recurrent = data;
      });
  }

  delete(content, recurrentName, recurrentID) {
    this.recurr = { name: recurrentName, eMail: recurrentID };
    this.modalService.open(content, { centered: true });
  }

  confirmDelete(administratorID, residenceID, residentID, recurrentID) {
    this.recurrentService.delete(administratorID, residenceID, residentID, recurrentID).then(f => {
      this.modalService.dismissAll();
    });
  }
}
