import { Component, OnInit } from '@angular/core';
import { ResidenceService } from '../services/residence.service';
import { ResidentService } from '../services/resident.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { Residence } from '../interfaces/residence';
import { Resident } from '../interfaces/resident';

@Component({
  selector: 'app-resident',
  templateUrl: './resident.component.html',
  styleUrls: ['./resident.component.scss']
})
export class ResidentComponent implements OnInit {
  residenceID: any;
  administratorID: any;
  residence: Residence;
  residents: Resident[];
  resident: Resident = { phoneCountryCode: 0, residenceCode: '', password: '', phone: 0, uid: '', status: 0, email: '', name: '' };
  closeResult: string;
  comments = '';
  constructor(
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private residentService: ResidentService,
    private residenceService: ResidenceService
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
  }

  ngOnInit() {
    this.getResidents();
    this.getResidenceInfo();
  }

  confirmResident(content, residentName, residentID) {
    this.updateResidentInfo(this.administratorID, this.residenceID, residentID, 2, '');
  }
  rejectResident(content2, residentName, residentID) {
    this.modalService.open(content2, { centered: true });
    this.resident.name = residentName;
    this.resident.uid = residentID;
  }

  confirmRejection(administratorID, residenceID, residentID) {
    this.updateResidentInfo(this.administratorID, this.residenceID, residentID, 3, this.comments);
  }

  updateResidentInfo(administratorID, residenceID, uid, status, comments) {
    this.residentService
      .getOne(administratorID, residenceID, uid)
      .valueChanges()
      .subscribe((data: Resident) => {
        this.resident = data;
        console.log(uid);
        this.resident.status = status;
        this.resident.comments = comments;
        this.residentService.set(administratorID, residenceID, this.resident);
        // this.residenceTypeService = result;
        // result.docs.forEach(i => console.log(i.get()));
        this.modalService.dismissAll();
      });
  }

  delete(content, residentName, residentID) {
    this.resident.name = residentName;
    this.resident.uid = residentID;
    this.modalService.open(content, { centered: true });
  }

  confirmDelete(administratorID, residenceID, residentID) {
    this.residentService.delete(administratorID, residenceID, residentID, this.comments).then(f => {
      this.modalService.dismissAll();
    });
  }

  getResidents() {
    this.residentService
      .get(this.administratorID, this.residenceID)
      .valueChanges()
      .subscribe((data: Resident[]) => {
        this.residents = data;
      });
  }

  private getResidenceInfo() {
    this.residenceService
      .get(this.administratorID, this.residenceID)
      .valueChanges()
      .subscribe((data: Residence) => {
        this.residence = data;
      });
  }
}
