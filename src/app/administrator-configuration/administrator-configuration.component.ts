import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ResidenceService } from '../services/residence.service';
import { Residence } from '../interfaces/residence';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdministratorConfiguration } from '../interfaces/administratorConfiguration';
import { AdministratorConfigurationService } from '../services/administrator-configuration.service';
import { ToastService } from '../services/toast.service';

@Component({
  selector: 'app-administrator-configuration',
  templateUrl: './administrator-configuration.component.html',
  styleUrls: ['./administrator-configuration.component.scss']
})
export class AdministratorConfigurationComponent implements OnInit {
  residenceID: any;
  administratorID: any;
  residence: Residence;
  form: FormGroup;
  submitted = false;
  frmVisitor: FormGroup;
  frmResident: FormGroup;
  frmResidence: FormGroup;
  generalConfiguration: AdministratorConfiguration;
  constructor(
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private configurationService: AdministratorConfigurationService,
    private residenceService: ResidenceService,
    private toastService: ToastService
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
  }

  ngOnInit() {
    this.frmResidence = this.createFormResidence();
    this.frmVisitor = this.createVisitorForm();
    this.frmResident = this.createResidentForm();
    this.getConfiguration(this.administratorID, this.residenceID);
    this.getResidenceInfo();
  }

  private getConfiguration(administratorID, residenceID) {
    // this.configurationService
    //   .get(administratorID, residenceID)
    //   .valueChanges()
    //   .subscribe((data: AdministratorConfiguration) => {
    //     this.residence.configuration = data;
    //   });
  }

  private getResidenceInfo() {
    this.residenceService
      .get(this.administratorID, this.residenceID)
      .valueChanges()
      .subscribe((data: Residence) => {
        // console.log(data);
        this.residence = data;
        this.frmVisitor.controls['documentImage'].setValue(this.residence.configuration.visitor.documentImage);
        this.frmVisitor.controls['identificationDocument'].setValue(this.residence.configuration.visitor.identificationDocument);
        this.frmVisitor.controls['badgeNumber'].setValue(this.residence.configuration.visitor.badgeNumber);
        this.frmVisitor.controls['email'].setValue(this.residence.configuration.visitor.email);
        this.frmVisitor.controls['phone'].setValue(this.residence.configuration.visitor.phone);
        this.frmResident.controls['residenceImage'].setValue(this.residence.configuration.register.residenceImage);
        this.frmResident.controls['residenceNumber'].setValue(this.residence.configuration.register.residenceNumber);
        this.frmResident.controls['streetNumber'].setValue(this.residence.configuration.register.streetNumber);
        this.frmResident.controls['phone'].setValue(this.residence.configuration.register.phone);
        this.frmResidence = this.createFormResidence();
      });
  }

  createFormResidence(): FormGroup {
    try {
      return this.fb.group({
        name: [this.residence.name, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(50)])],
        address: [this.residence.address, Validators.compose([Validators.required])],
        residenceType: [this.residence.type, Validators.compose([Validators.required])]
      });
    } catch (error) {}
  }

  createVisitorForm(): FormGroup {
    return this.fb.group({
      documentImage: [true, Validators.compose([Validators.required])],
      identificationDocument: [true, Validators.compose([Validators.required])],
      badgeNumber: [true, Validators.compose([Validators.required])],
      email: [true, Validators.compose([Validators.required])],
      phone: [true, Validators.compose([Validators.required])]
    });
  }

  createResidentForm(): FormGroup {
    return this.fb.group({
      residenceImage: [true, Validators.compose([Validators.required])],
      residenceNumber: [true, Validators.compose([Validators.required])],
      streetNumber: [true, Validators.compose([Validators.required])],
      phone: [true, Validators.compose([Validators.required])]
    });
  }

  saveRegisterConfiguration() {
    this.residence.configuration.register.phone = this.frmResident.controls['phone'].value;
    this.residence.configuration.register.residenceImage = this.frmResident.controls['residenceImage'].value;
    this.residence.configuration.register.residenceNumber = this.frmResident.controls['residenceNumber'].value;
    this.residence.configuration.register.streetNumber = this.frmResident.controls['streetNumber'].value;
    this.residence.configuration.visitor.phone = this.frmVisitor.controls['phone'].value;
    this.residence.configuration.visitor.identificationDocument = this.frmVisitor.controls['identificationDocument'].value;
    this.residence.configuration.visitor.email = this.frmVisitor.controls['email'].value;
    this.residence.configuration.visitor.documentImage = this.frmVisitor.controls['documentImage'].value;
    this.residence.configuration.visitor.badgeNumber = this.frmVisitor.controls['badgeNumber'].value;
    this.residenceService.set(this.residence, this.administratorID).then(result => {
      this.toastService.show('Configuration saved successfully', { classname: 'bg-success text-light my-5', delay: 5000 });
    });
  }

  saveResidentInformation() {
    this.residence.name = this.frmResidence.controls['name'].value;
    this.residence.address = this.frmResidence.controls['address'].value;
    this.residence.type = this.frmResidence.controls['residenceType'].value;
    this.residenceService.set(this.residence, this.administratorID).then(result => {
      this.toastService.show('Information saved successfully', { classname: 'bg-success text-light  my-5', delay: 5000 });
    });
  }

  
}
