import { Component, OnInit } from '@angular/core';
import { GuardService } from '../services/guard.service';
import { ActivatedRoute } from '@angular/router';
import { Code } from '../interfaces/code';
import { ResidenceService } from '../services/residence.service';
import { ResidentService } from '../services/resident.service';
import { Residence } from '../interfaces/residence';
import { Resident } from '../interfaces/resident';

@Component({
  selector: 'app-report-visit-day',
  templateUrl: './report-visit-day.component.html',
  styleUrls: ['./report-visit-day.component.scss']
})
export class ReportVisitDayComponent implements OnInit {
  administratorID: any;
  residenceID: any;
  guardID: any;
  codes: Code[] = [];
  code: Code;
  dateToday;
  constructor(
    private residentService: ResidentService,
    private residenceService: ResidenceService,
    private guardService: GuardService,
    private activatedRoute: ActivatedRoute
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.guardID = this.activatedRoute.snapshot.params[param3];
  }

  ngOnInit() {
    const now = Date.now();
    let dt = new Date(now);
    dt = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
    this.dateToday = dt.getTime();
    let report = this.guardService.getAccessCodeToday(this.administratorID, this.residenceID, dt.getTime());
    report.then(result => {
      result.docs.forEach(r => {
        // console.log(r.data());
        this.codes.push({ ...r.data() });
      });
      this.codes.forEach(i => {
        this.residenceService
          .get(this.administratorID, this.residenceID)
          .valueChanges()
          .subscribe((data: Residence[]) => (i.Residence = data));
        this.residentService
          .getOne(this.administratorID, this.residenceID, i.residentID)
          .valueChanges()
          .subscribe((data: Resident[]) => {
            i.Resident = data;
          });
      });
      // console.log(this.codes);
    });
  }
}
