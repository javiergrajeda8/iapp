import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportVisitDayComponent } from './report-visit-day.component';

describe('ReportVisitDayComponent', () => {
  let component: ReportVisitDayComponent;
  let fixture: ComponentFixture<ReportVisitDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportVisitDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportVisitDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
