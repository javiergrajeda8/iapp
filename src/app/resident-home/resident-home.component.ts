import { Component, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthorizationService } from '../services/authorization.service';
import { ResidentService } from '../services/resident.service';
import { Resident } from '../interfaces/resident';
import { Residence } from '../interfaces/residence';
import { ResidenceService } from '../services/residence.service';
import { Visitor } from '../interfaces/visitor';
import { Recurrent } from '../interfaces/recurrent';
import { Poll } from '../interfaces/poll';
import { PollService } from '../services/poll.service';
import { ToastService } from '../services/toast.service';

@Component({
  selector: 'app-resident-home',
  templateUrl: './resident-home.component.html',
  styleUrls: ['./resident-home.component.scss']
})
export class ResidentHomeComponent implements OnInit {
  administratorID: any;
  residenceID: any;
  resident: Resident;
  residence: Residence;
  uid: 0;
  equal = false;
  constructor(
    private auth: AuthorizationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private residentService: ResidentService,
    private residenceService: ResidenceService,
    private pollService: PollService,
    private toastService: ToastService
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.uid = this.activatedRoute.snapshot.params[param3];
    if (this.uid != 0) {
      // console.log(this.uid);
      this.getResidentInfo(this.administratorID, this.residenceID, this.uid);
    }
  }

  ngOnInit() {
    this.getResidenceInfo(this.administratorID, this.residenceID);
    // this.getResidentInfo(this.administratorID, this.residenceID, this.uid);
  }

  // getResidentInfo(administratorID, residenceID, residentID) {}

  getResidenceInfo(administratorID, residenceID) {
    this.residenceService
      .get(administratorID, residenceID)
      .valueChanges()
      .subscribe((data: Residence) => {
        this.residence = data;
      });
  }

  goVisitor() {
    if (this.resident.status == 2) {
      this.router.navigate(['newVisit/' + this.administratorID + '/' + this.residenceID + '/' + this.uid + '/0']);
    } else {
      this.router.navigate(['residentHome/' + this.administratorID + '/' + this.residenceID + '/' + this.uid]);
    }
  }

  getResidentInfo(administratorID, residenceID, uid) {
    const residentQuery = this.residentService.getOne(administratorID, residenceID, uid);

    residentQuery.valueChanges().subscribe((data: Resident) => {
      this.resident = data;
      this.auth.getUID().then(result => {
        if (result.uid === this.resident.uid || this.uid != 0) {
          if (result.uid === this.resident.uid) {
            this.equal = true;
            if (this.resident.status === 0) {
              this.router.navigate(['residentForm/' + this.administratorID + '/' + this.residenceID + '/' + this.uid]);
            } else {
              residentQuery
                .collection('visitor')
                .valueChanges()
                .subscribe((data2: Visitor[]) => {
                  this.resident.Visitor = data2.filter(fn => fn.status === 1);
                  residentQuery
                    .collection('recurrent')
                    .valueChanges()
                    .subscribe((data3: Recurrent[]) => {
                      this.resident.Recurrent = data3.filter(fn => fn.status === 1);
                      // console.log(this.resident);
                    });
                });
            }
          }
        }
        // console.log('LOGGED UID', result.uid);
        // console.log('UID TO EDIT', this.resident.uid);
      });
    });
  }

  poll(residence, role, option, like) {
    const POLL: Poll = { role, option, like, reference: 'administrator/' + this.uid + '/residence/' + residence + '/resident/' + this.uid };
    this.pollService.set(POLL).then(result => {
      this.toastService.show('Thanks!', { classname: 'bg-success text-light my-5', delay: 5000 });
    });
  }
}
