import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../services/authorization.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-forget-password',
  templateUrl: './login-forget-password.component.html',
  styleUrls: ['./login-forget-password.component.scss']
})
export class LoginForgetPasswordComponent implements OnInit {
  email = '';
  msj = '';
  msjOk = '';
  public frmSignup: FormGroup;
  constructor(private fb: FormBuilder, private auth: AuthorizationService) {}

  ngOnInit() {
    this.frmSignup = this.createSignupForm();
  }

  createSignupForm(): FormGroup {
    return this.fb.group({
      email: [null, Validators.compose([Validators.email, Validators.required])]
    });
  }

  resetPassword() {
    this.msj = '';
    this.msjOk = '';
    this.auth.sendEmail(
      this,
      this.frmSignup.controls['email'].value,
      function success(context) {
        context.msj = '';
        context.msjOk = 'Please check your email inbox to reset your password';
        context.frmSignup.controls['email'].setValue('');
      },
      function error(context, er) {
        if ((er.code = 'auth/invalid-email')) {
          context.msj = 'The email have not been found, please verified';
          context.msjOk = '';
        }
      }
    );
  }
}
