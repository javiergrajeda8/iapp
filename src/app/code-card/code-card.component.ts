import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResidentService } from '../services/resident.service';
import { ResidenceService } from '../services/residence.service';
import { Residence } from '../interfaces/residence';
import { Resident } from '../interfaces/resident';
import { VisitorService } from '../services/visitor.service';
import { Visitor } from '../interfaces/visitor';
import { Guard } from '../interfaces/guard';
import { CodeService } from '../services/code.service';
import { Code } from '../interfaces/code';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GuardService } from '../services/guard.service';

@Component({
  selector: 'app-code-card',
  templateUrl: './code-card.component.html',
  styleUrls: ['./code-card.component.scss']
})
export class CodeCardComponent implements OnInit {
  residenceID: any;
  residence: Residence;
  administratorID: any;
  residentID: any;
  resident: Resident;
  visitID: any;
  visitor: Visitor;
  guardID: any;
  guard: Guard;
  comments = '';
  reason = '';
  constructor(
    private activatedRoute: ActivatedRoute,
    private residentService: ResidentService,
    private residenceService: ResidenceService,
    private visitorService: VisitorService,
    private router: Router,
    private modalService: NgbModal,
    private codeService: CodeService,
    private guardService: GuardService
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.guardID = this.activatedRoute.snapshot.params[param3];
    if (!this.residentID) {
      this.activatedRoute.queryParams.subscribe(f => {
        this.residentID = f.residentID;
      });
    }
    if (!this.visitID) {
      this.activatedRoute.queryParams.subscribe(f => {
        this.visitID = f.visitID;
      });
    }
  }

  ngOnInit() {
    this.getResidentInfo(this.administratorID, this.residenceID, this.residentID);
    this.getResidenceInfo(this.administratorID, this.residenceID);
    this.getVisitorInfo(this.administratorID, this.residenceID, this.residentID, this.visitID);
    this.getGuardInfo(this.administratorID, this.residenceID, this.guardID);
  }

  reject(content) {
    this.modalService.open(content, { centered: true });
  }

  rejectConfirm() {
    this.visitorService
      .reject(this.administratorID, this.residenceID, this.residentID, this.visitID, this.reason, this.comments, this.guard)
      .then(result => {
        const code: Code = {
          value: this.visitor.code,
          residenceID: this.residenceID,
          residentID: this.residentID,
          visitorID: this.visitID,
          status: 3,
          name: this.visitor.name,
          visitDate: this.visitor.visitDate,
          rejectReason: this.reason,
          rejectObservation: this.comments,
          Guard: this.guard
        };
        this.codeService
          .reject(this.administratorID, this.residenceID, code)
          .then(result2 => {
            const me = this;
            setTimeout(timer => {
              me.router.navigate(['reportVisitDay/' + me.administratorID + '/' + me.residenceID + '/' + me.guardID]);
            }, 1000);
          })
          .catch(error => console.log(error));
      })
      .catch(error => console.log(error));
  }

  confirm() {
    this.visitorService.confirm(this.administratorID, this.residenceID, this.residentID, this.visitID, this.guard).then(result => {
      const code: Code = {
        value: this.visitor.code,
        residenceID: this.residenceID,
        residentID: this.residentID,
        visitorID: this.visitID,
        status: 2,
        name: this.visitor.name,
        visitDate: this.visitor.visitDate,
        Guard: this.guard
      };
      this.codeService.confirm(this.administratorID, this.residenceID, code).then(result2 => {
        const me = this;
        setTimeout(timer => {
          me.router.navigate(['reportVisitDay/' + me.administratorID + '/' + me.residenceID + '/' + me.guardID]);
        }, 1000);
      });
    });
  }

  getGuardInfo(administratorID, residenceID, guardID) {
    this.guardService
      .getOne(administratorID, residenceID, guardID)
      .valueChanges()
      .subscribe((data: Guard) => {
        this.guard = data;
      });
  }

  getResidenceInfo(administratorID, residenceID) {
    this.residenceService
      .get(administratorID, residenceID)
      .valueChanges()
      .subscribe((data: Residence) => {
        this.residence = data;
      });
  }

  getResidentInfo(administratorID, residenceID, uid) {
    this.residentService
      .getOne(administratorID, residenceID, uid)
      .valueChanges()
      .subscribe((data: Resident) => {
        this.resident = data;
      });
  }

  getVisitorInfo(administratorID, residenceID, residentID, visitID) {
    this.visitorService
      .getOne(administratorID, residenceID, residentID, visitID)
      .valueChanges()
      .subscribe((data: Visitor) => {
        this.visitor = data;
        this.getImage(this.visitor.documentImage);
      });
  }

  getImage(visitID) {
    this.visitorService.getDocument('documents', visitID).subscribe(p => {
      this.visitor.documentImage = p;
    });
  }
}
