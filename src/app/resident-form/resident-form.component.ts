import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Resident } from '../interfaces/resident';
import { ActivatedRoute, Router } from '@angular/router';
import { ResidentService } from '../services/resident.service';
import { CustomValidators } from '../services/custom-validators';
import { ResidenceService } from '../services/residence.service';
import { Residence } from '../interfaces/residence';
import { AuthorizationService } from '../services/authorization.service';
import { User } from '../interfaces/user';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-resident-form',
  templateUrl: './resident-form.component.html',
  styleUrls: ['./resident-form.component.scss']
})
export class ResidentFormComponent implements OnInit {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  residenceID: any;
  administratorID: any;
  frmResident: FormGroup;
  resident: Resident;
  residence: Residence;
  uid: 0;
  equal = false;
  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private auth: AuthorizationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private residentService: ResidentService,
    private residenceService: ResidenceService
  ) {
    const param1 = 'uid';
    const param2 = 'uid2';
    const param3 = 'uid3';
    this.administratorID = this.activatedRoute.snapshot.params[param1];
    this.residenceID = this.activatedRoute.snapshot.params[param2];
    this.uid = this.activatedRoute.snapshot.params[param3];
  }

  ngOnInit() {
    this.frmResident = this.createResidentForm();
    this.frmResident.controls['residenceCode'].setValue(this.residenceID);
    if (this.uid != 0) {
      // console.log(this.uid);
      this.getResidentInfo(this.administratorID, this.residenceID, this.uid);
    } else {
      this.frmResident.controls['image'].setValidators(null);
      this.frmResident.controls['image'].setValue('');
      this.frmResident.updateValueAndValidity();
    }
    this.getResidenceInfo(this.administratorID, this.residenceID);
  }

  getResidenceInfo(administratorID, residenceID) {
    this.residenceService
      .get(administratorID, residenceID)
      .valueChanges()
      .subscribe((data: Residence) => {
        this.residence = data;
        this.verificateValidators(this.residence, this.frmResident);
      });
  }

  private verificateValidators(residence: Residence, frm) {
    if (residence.configuration.register.residenceImage) {
      frm.controls['image'].setValidators(Validators.compose([Validators.required]));
    } else {
      frm.controls['image'].setValidators(null);
    }
    frm.controls['image'].updateValueAndValidity();
    if (residence.configuration.register.residenceNumber) {
      frm.controls['residenceNumber'].setValidators(Validators.compose([Validators.required]));
    } else {
      frm.controls['residenceNumber'].setValidators(null);
    }
    frm.controls['residenceNumber'].updateValueAndValidity();
    if (residence.configuration.register.streetNumber) {
      frm.controls['street'].setValidators(Validators.compose([Validators.required]));
    } else {
      frm.controls['street'].setValidators(null);
    }
    frm.controls['street'].updateValueAndValidity();
    if (residence.configuration.register.phone) {
      frm.controls['code'].setValidators(
        Validators.compose([Validators.required, Validators.max(999), Validators.pattern(/^-?(0|[1-9]\d*)?$/)])
      );
      frm.controls['number'].setValidators(Validators.compose([Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]));
    } else {
      frm.controls['code'].setValidators(null);
      frm.controls['number'].setValidators(null);
    }
    frm.controls['code'].updateValueAndValidity();
    frm.controls['number'].updateValueAndValidity();
  }

  getImage(imageID) {
    this.residentService.getDocument('resident', imageID).subscribe(p => {
      console.log(p);
      this.croppedImage = p;
      // this.resident.image = p;
    });
  }

  getResidentInfo(administratorID, residenceID, uid) {
    this.residentService
      .getOne(administratorID, residenceID, uid)
      .valueChanges()
      .subscribe((data: Resident) => {
        this.resident = data;
        // console.log(this.resident);
        this.frmResident.controls['email'].setValue(this.resident.email);
        this.frmResident.controls['level'].setValue(this.resident.level);
        this.frmResident.controls['streetType'].setValue(this.resident.streetType);
        this.frmResident.controls['street'].setValue(this.resident.street);
        this.frmResident.controls['residenceNumber'].setValue(this.resident.residenceNumber);
        this.frmResident.controls['residenceType'].setValue(this.resident.residenceType);
        this.frmResident.controls['name'].setValue(this.resident.name);
        this.frmResident.controls['code'].setValue(this.resident.phoneCountryCode);
        this.frmResident.controls['number'].setValue(this.resident.phone);
        this.frmResident.controls['residenceCode'].setValue(this.resident.residenceCode);
        this.frmResident.controls['image'].setValue(this.resident.image);
        this.getImage(this.resident.image);
        this.auth.getUID().then(result => {
          if (result.uid === this.resident.uid || this.uid != 0) {
            if (result.uid === this.resident.uid) {
              this.equal = true;
            }
            this.frmResident.controls['password'].setValidators(null);
            this.frmResident.controls['confirmPassword'].setValidators(null);
            this.frmResident.controls['password'].updateValueAndValidity();
          }
          // console.log('LOGGED UID', result.uid);
          // console.log('UID TO EDIT', this.resident.uid);
        });
      });
  }

  createResidentForm(): FormGroup {
    return this.fb.group(
      {
        uid: [null, null],
        image: [null, Validators.compose([Validators.required])],
        email: [null, Validators.compose([Validators.required, Validators.email])],
        name: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(50)])],
        level: [null, Validators.compose([Validators.min(-20), Validators.max(999), Validators.pattern(/^-?(0|[1-9]\d*)?$/)])],
        streetType: ['Street', Validators.compose([Validators.required])],
        street: [null, Validators.compose([Validators.required])],
        residenceCode: [null, null],
        residenceNumber: [null, Validators.compose([Validators.required])],
        residenceType: ['House', Validators.compose([Validators.required])],
        code: [null, Validators.compose([Validators.required, Validators.max(999), Validators.pattern(/^-?(0|[1-9]\d*)?$/)])],
        number: [null, Validators.compose([Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)])],
        password: [
          null,
          Validators.compose([
            Validators.required,
            // check whether the entered password has a number
            CustomValidators.patternValidator(/\d/, {
              hasNumber: true
            }),
            // check whether the entered password has upper case letter
            CustomValidators.patternValidator(/[A-Z]/, {
              hasCapitalCase: true
            }),
            // check whether the entered password has a lower case letter
            CustomValidators.patternValidator(/[a-z]/, {
              hasSmallCase: true
            }),
            // check whether the entered password has a special character
            CustomValidators.patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, {
              hasSpecialCharacters: true
            }),
            Validators.minLength(8)
          ])
        ],
        confirmPassword: [null]
      },
      {
        // check whether our password and confirm password match
        validator: CustomValidators.passwordMatchValidator
      }
    );
  }

  save() {
    let routeToNavigate = 'resident';
    let res: Resident = {
      uid: this.uid.toString(),
      email: this.frmResident.controls['email'].value,
      level: this.frmResident.controls['level'].value,
      streetType: this.frmResident.controls['streetType'].value,
      street: this.frmResident.controls['street'].value,
      residenceNumber: this.frmResident.controls['residenceNumber'].value,
      residenceType: this.frmResident.controls['residenceType'].value,
      name: this.frmResident.controls['name'].value,
      phoneCountryCode: this.frmResident.controls['code'].value,
      phone: this.frmResident.controls['number'].value,
      residenceCode: this.frmResident.controls['residenceCode'].value,
      password: this.frmResident.controls['password'].value,
      status: 2
    };
    console.log(this.uid);
    if (this.uid == 0) {
      res.image = 0;
      this.resident = res;
      this.auth.register(
        this,
        res.email,
        res.password,
        function success(context, usr: User) {
          context.resident.uid = usr.uid;
          context.resident.password = '';
          context.resident.status = 2;
          // console.log('resident', context.resident);
          context.residentService.set(context.administratorID, context.residenceID, context.resident, callback => {
            context.router.navigate([routeToNavigate + '/' + context.administratorID + '/' + context.residenceID]);
          });
        },
        function error(context: any, error: any) {
          console.log(error);
        }
      );
    } else {
      this.resident.password = '';
      if (this.equal) {
        routeToNavigate = 'residentHome';
        if (this.resident.status == 0) {
          this.resident = res;
          this.resident.status = 1;
        } else {
          this.resident = res;
        }
      }

      //  console.log(routeToNavigate);
      if (this.frmResident.controls['image'].touched) {
        res.image = Date.now();
        this.residentService.saveDocument(this.resident.image, this.croppedImage, 'data_url').then(result => {});
      }
      this.residentService.set(this.administratorID, this.residenceID, this.resident).then(result => {
        this.router.navigate([routeToNavigate + '/' + this.administratorID + '/' + this.residenceID + '/' + res.uid]);
      });
    }
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  uploadImage(content) {
    this.modalService.open(content, { centered: true });
  }
}
