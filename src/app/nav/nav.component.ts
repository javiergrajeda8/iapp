import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthorizationService } from '../services/authorization.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  loggedIn = false;
  uid;
  email = '';
  menu = '';
  constructor(private auth: AuthorizationService, private router: Router) {}

  ngOnInit() {
    this.initializeVariables();
  }

  initializeVariables() {
    this.auth.isLogged().subscribe(
      result => {
        // console.log(result);
        if (result && result.uid) {
          this.loggedIn = true;
          this.email = result.email;
          this.uid = result.uid;
          this.getMenu(this.auth, this, this.email);
        } else {
          this.loggedIn = false;
        }
      },
      error => {
        this.loggedIn = false;
      }
    );
  }

  getMenu(auth, router, user) {
    auth.searchUserType2(router, user, function callback(cxt, usrType: firebase.firestore.QuerySnapshot) {
      // console.log('hello', usrType);
      usrType.forEach(i => {
        if (i.ref.parent.id == 'administrator') {
          cxt.menu = 'administratorAdministration/' + i.id;
        } else {
          // console.log(i);

          cxt.menu = 'residentAdministration/' + i.ref.parent.parent.parent.parent.id + '/' + i.ref.parent.parent.id + '/' + i.id;
          // console.log(cxt.menu);
          // cxt.navigate(['residentHome/' + i.ref.parent.parent.parent.parent.id + '/' + i.ref.parent.parent.id + '/' + i.ref.id]);
        }
      });
    });
  }

  logOut() {
    this.auth.logOut();
  }
}
