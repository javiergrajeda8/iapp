import { DecimalPipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';

import { CountryService } from '../services/country.service';
import { NgbdSortableHeader, SortEvent } from '../directive/sortable.directive';
import { Country } from '../interfaces/country';

@Component({
  selector: 'ngbd-table-complete',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
  providers: [CountryService, DecimalPipe]
})
export class NgbdTableComplete {
  countries$: Observable<Country[]>;
  total$: Observable<number>;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(public service: CountryService) {
    this.countries$ = service.countries$;
    this.total$ = service.total$;
  }

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }
}
