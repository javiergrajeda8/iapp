import { TestBed } from '@angular/core/testing';

import { RecurrentService } from './recurrent.service';

describe('RecurrentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecurrentService = TestBed.get(RecurrentService);
    expect(service).toBeTruthy();
  });
});
