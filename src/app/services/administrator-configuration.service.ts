import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Residence } from '../interfaces/residence';

@Injectable({
  providedIn: 'root'
})
export class AdministratorConfigurationService {
  constructor(private firestore: AngularFirestore) {}

  get(administratorID, residenceID) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('configuration')
      .doc(residenceID);
  }

  set(administratorID, residence: Residence) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residence.code)
      .collection('configuration')
      .doc(residence.code)
      .set(residence.configuration);
  }
}
