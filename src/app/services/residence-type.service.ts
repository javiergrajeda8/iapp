import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResidenceTypeService {
  constructor(private firestore: AngularFirestore) {}

  public getAll(): AngularFirestoreCollection<unknown> {
    return this.firestore.collection('residenceType');
  }
}
