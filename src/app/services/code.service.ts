import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Code } from '../interfaces/code';
import { Guard } from '../interfaces/guard';

@Injectable({
  providedIn: 'root'
})
export class CodeService {
  constructor(private firestore: AngularFirestore) {}

  public confirm(administratorID, residenceID, code: Code) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('code')
      .doc(code.value)
      .set(code);
      // .update({ status: 2 });
  }

  public reject(administratorID, residenceID, code: Code) {
    // console.log(administratorID, residenceID, code);
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('code')
      .doc(code.value)
      .set(code);
  }

  public set(administratorID, residenceID, code: Code, callback) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('code')
      .doc(code.value)
      .set(code)
      .then(r => {
        callback();
      })
      .catch(e => {
        alert('Ocurrió un error 2');
        console.log(e);
      });
  }
}
