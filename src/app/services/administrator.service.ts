import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Administrator } from '../interfaces/administrator';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdministratorService {
  constructor(private afDB: AngularFireDatabase, private firestore: AngularFirestore) {}

  public new(administrator: Administrator) {
    // console.log(administrator);
    this.firestore
      .collection('administrator')
      .doc(administrator.uid)
      .set(administrator)
      .then(result => console.log(result))
      .catch(error => console.log(error));
    // this.afDB.database.ref('/administrator/' + administrator.uid).set(administrator);
    // this.afDB.database.app.firestore.
  }

  public searchEmail(email): Observable<firebase.firestore.QuerySnapshot> {
    return this.firestore.collection('administrator/*', ref => ref.where('email', '==', email)).get();
  }

  public list() {
    return this.firestore.collection('administrator');
  }

  public get(uid): AngularFirestoreDocument {
    // console.log(uid);
    return this.firestore.collection('administrator').doc(uid);
  }

  public update(administratorID, values) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .update(values);
  }

  public set(administrator: Administrator) {
    return this.firestore
      .collection('administrator')
      .doc(administrator.uid)
      .set(administrator);
  }

  public delete(uid) {
    return this.firestore
      .collection('administrator')
      .doc(uid)
      .delete();
  }
}
