import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Guard } from '../interfaces/guard';
import { ConstantService } from './constant.service';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class GuardService {
  constructor(private firestore: AngularFirestore, private consts: ConstantService) {}

  public getAccessCodeToday(administratorID, residenceID, datetoday) {
    // console.lob g(residenceID, datetoday);
    return firebase
      .firestore()
      .collectionGroup('code')
      .where('visitDate', '==', datetoday)
      .where('residenceID', '==', residenceID)
      .orderBy('status')
      .get();

    // .snapshotChanges()
    // .subscribe(r => {
    //   console.log(r);
    // });
  }

  public getAccessCodeLastWeek(administratorID, residenceID, dateTo, dateFrom) {
    // console.log(residenceID, dateFrom, dateTo);
    return firebase
      .firestore()
      .collectionGroup('code')
      .where('visitDate', '>=', dateFrom)
      .where('visitDate', '<=', dateTo)
      .where('residenceID', '==', residenceID)
      .orderBy('visitDate').orderBy('status')
      .get();

    // .snapshotChanges()
    // .subscribe(r => {
    //   console.log(r);
    // });
  }

  public validAccessCode(administratorID, residenceID, residentID, visitorID, code, visitDate) {
    // console.log(administratorID,residenceID, residentID, visitorID, code, visitDate);
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .collection('visitor', ref =>
        ref
          .where('status', '==', 1)
          .where('code', '==', code)
          .where('visitDate', '==', visitDate)
      )
      .doc(visitorID);
  }

  public searchAccessCode(administratorID, residenceID, code) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('code', ref => ref.where('status', '==', 1).where('value', '==', code));
  }

  public get(administratorID, residenceID): AngularFirestoreCollection<unknown> {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('guard', ref => ref.where('status', '==', 1));
  }

  public getOne(administratorID, residenceID, guardID): AngularFirestoreDocument<unknown> {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('guard', ref => ref.where('status', '==', 1))
      .doc(guardID);
  }

  public new(administratorID, residenceID, guard: Guard, callback) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('guard')
      .doc(guard.uid)
      .set(guard)
      .then(r => {
        callback();
      })
      .catch(e => {
        alert('Ocurrió un error');
        console.log(e);
      });
  }

  public delete(administratorID, residenceID, guardID) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('guard')
      .doc(guardID)
      .update({ status: this.consts.getDeleteStatus() });
  }
}
