import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Visitor } from '../interfaces/visitor';
import { AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { Guard } from '../interfaces/guard';

@Injectable({
  providedIn: 'root'
})
export class VisitorService {
  constructor(private firestore: AngularFirestore, private storage: AngularFireStorage) {}

  public confirm(administratorID, residenceID, residentID, visitorID, guard: Guard) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .collection('visitor')
      .doc(visitorID)
      .update({ status: 2, confirmDate: Date.now(), Guard: guard });
  }

  public reject(administratorID, residenceID, residentID, visitorID, reason, observation, guard: Guard) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .collection('visitor')
      .doc(visitorID)
      .update({ status: 3, rejectReason: reason, rejectObservation: observation, confirmDate: Date.now(), Guard: guard });
  }

  public saveDocument(imageName, image, data): AngularFireUploadTask {
    return this.storage.ref('documents/' + imageName + '.png').putString(image, data);
  }

  public getDocument(ubication, imageName): Observable<any> {
    return this.storage.ref(ubication + '/' + imageName + '.png').getDownloadURL();
  }

  public set(context, administratorID, residenceID, residentID, visitor: Visitor, callback) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .collection('visitor')
      .doc(visitor.documentImage.toString())
      .set(visitor)
      .then(r => {
        callback(context, visitor);
      })
      .catch(e => {
        alert('Ocurrió un error');
        console.log(e);
      });
  }

  public getOne(administratorID, residenceID, residentID, visitorCode): AngularFirestoreDocument<unknown> {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .collection('visitor')
      .doc(visitorCode);
  }

  public get(administratorID, residenceID, residentID) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .collection('visitor');
  }

  public delete(administratorID, residenceID, residentID, visitorID) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .collection('visitor')
      .doc(visitorID)
      .update({ status: 99 });
  }
}
