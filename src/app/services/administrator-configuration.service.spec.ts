import { TestBed } from '@angular/core/testing';

import { AdministratorConfigurationService } from './administrator-configuration.service';

describe('AdministratorConfigurationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdministratorConfigurationService = TestBed.get(AdministratorConfigurationService);
    expect(service).toBeTruthy();
  });
});
