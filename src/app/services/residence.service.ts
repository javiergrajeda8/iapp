import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Residence } from '../interfaces/residence';
import { Observable } from 'rxjs';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class ResidenceService {
  constructor(private firestore: AngularFirestore) {}

  public searchResidence(code) {
    return firebase
      .firestore()
      .collectionGroup('residence')
      .where('code', '==', code)
      .get();
  }

  public set(residence: Residence, administratoruid: string) {
    // console.log(residence);
    return this.firestore
      .collection('administrator')
      .doc(administratoruid)
      .collection('residence')
      .doc(residence.code)
      .set(residence);
      // .then(result => {
      //   // console.log(result);
      //   callback();
      // })
      // .catch(error => console.log(error));
    // this.afDB.database.ref('/administrator/' + administrator.uid).set(administrator);
    // this.afDB.database.app.firestore.
  }

  public get(administrorID, residenceID): AngularFirestoreDocument<unknown> {
    return this.firestore
      .collection('administrator')
      .doc(administrorID)
      .collection('residence')
      .doc(residenceID);
  }
}
