import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Poll } from '../interfaces/poll';

@Injectable({
  providedIn: 'root'
})
export class PollService {
  constructor(private firestore: AngularFirestore) {}

  set(poll: Poll) {
    return this.firestore.collection('poll').add(poll);
  }
}
