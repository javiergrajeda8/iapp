import { Injectable } from '@angular/core';
import { AuthorizationService } from './authorization.service';

@Injectable({
  providedIn: 'root'
})
export class ConstantService {
  loggedIn = false;
  email = '';
  private deleteStatus = 99;
  constructor(private auth: AuthorizationService) {}

  getDeleteStatus() {
    return this.deleteStatus;
  }
}
