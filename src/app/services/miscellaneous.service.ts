import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MiscellaneousService {
  code = '';
  constructor(private firestore: AngularFirestore) {}

  public getCode(){
    const rnd = (Math.random() * 10).toFixed(0);
    // console.log(rnd);
    return this.firestore
      .collection('code')
      .doc(rnd.toString())
      .valueChanges()
      .pipe(take(1))
      .toPromise();
  }
}
