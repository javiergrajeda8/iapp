import { Injectable } from '@angular/core';
import { Visitor } from '../interfaces/visitor';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class RecurrentService {
  constructor(private firestore: AngularFirestore) {}

  public getOne(administratorID, residenceID, residentID, recurrentID) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .collection('recurrent')
      .doc(recurrentID);
  }

  public get(administratorID, residenceID, residentID) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .collection('recurrent', ref => ref.where('status', '==', 1));
  }

  public set(context, administratorID, residenceID, residentID, visitor: Visitor, callback) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .collection('recurrent')
      .doc(visitor.eMail)
      .set(visitor)
      .then(r => {
        callback(context, visitor);
      })
      .catch(e => {
        alert('Ocurrió un error');
        console.log(e);
      });
  }

  public delete(administratorID, residenceID, residentID, recurrentID) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .collection('recurrent')
      .doc(recurrentID)
      .update({status: 99});
  }
}
