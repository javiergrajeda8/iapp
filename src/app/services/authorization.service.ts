import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  public uid = '';
  public email = '';
  public name = '';
  public msj = 'test';
  constructor(private angularFireAuth: AngularFireAuth, private router: Router) {
    this.isLogged();
  }

  public checkEmailUnique() {
    return true;
  }

  public sendEmail(context, email, callback, callbackError) {
    this.angularFireAuth.auth
      .sendPasswordResetEmail(email)
      .then(result => {
        callback(context);
      })
      .catch(error => {
        console.log(error);
        callbackError(context, error);
      });
  }

  public loginGuard(context, email, password, callback, callbackError) {
    firebase
      .firestore()
      .collectionGroup('guard')
      .where('email', '==', email)
      .get()
      .then(result => {
        let resp = false;
        let administratorID = '';
        let residenceID = '';
        if (!result.empty) {
          const usr = result.docs.filter(f => f.get('password') == password && f.get('status') == 1);
          if (usr.length > 0) {
            administratorID = usr[0].ref.parent.parent.parent.parent.id;
            residenceID = usr[0].ref.parent.parent.id;
            // console.log(administratorID, residenceID, usr[0].id);
            resp = true;
          } else {
            resp = false;
          }
        } else {
          resp = false;
        }
        callback(context, resp, administratorID, residenceID, email);
      })
      .catch(error => {
        alert(error);
        callbackError(context, error);
      });
  }

  public searchUserType2(context, email, callback) {
    let userType = 'administrator';
    firebase
      .firestore()
      .collectionGroup('administrator')
      .where('email', '==', email)
      .get()
      .then(result => {
        // console.log('searchAdministrator', result, result.docs.length);
        if (result.docs.length > 0) {
          userType = 'administrator';
          callback(context, result);
        } else {
          firebase
            .firestore()
            .collectionGroup('resident')
            .where('email', '==', email)
            .get()
            .then(result2 => {
              if (result2.docs.length > 0) {
                // console.log('searchResident', result2, result2.docs.length);
                userType = 'resident';
                callback(context, result2);
              }
            })
            .catch(error => {
              console.log(error);
            });
        }
      });
  }

  public loginWithGoogle(context, callbackSuccess, callbackError) {
    this.angularFireAuth.auth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(result => {
        this.uid = result.user.uid;
        this.email = result.user.email;
        this.name = result.user.displayName;
        const usr: User = { name: this.name, uid: this.uid, email: this.email };
        callbackSuccess(context, usr);
      })
      .catch(error => {
        callbackError(context, error);
      });
  }

  public loginWithFacebook(context, callbackSuccess, callbackError) {
    this.angularFireAuth.auth
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(result => {
        // console.log(result);
        this.uid = result.user.uid;
        this.name = result.user.displayName;
        this.email = result.user.email;
        const usr: User = { name: this.name, uid: this.uid, email: this.email };
        callbackSuccess(context, usr);
      })
      .catch(error => {
        callbackError(context, error);
      });
  }

  public login(email, password, context, callback, callbackError) {
    this.angularFireAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(response => {
        // tslint:disable-next-line: no-debugger
        // debugger;
        this.uid = response.user.uid;
        this.name = response.user.displayName;
        this.email = response.user.email;
        const usr: User = { name: this.name, uid: this.uid, email: this.email };
        // console.log(response);
        callback(context, usr);
      })
      .catch(error => {
        callbackError(context, error);
        // alert('Un error ha ocurrido');
        // console.log(error);
      });
  }

  public register(context, email, password, callbackSuccess, callbackError) {
    this.angularFireAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then(response => {
        this.uid = response.user.uid;
        this.name = response.user.displayName;
        this.email = response.user.email;
        const usr: User = { name: this.name, uid: this.uid, email: this.email };
        // console.log(usr);
        callbackSuccess(context, usr);
      })
      .catch(error => {
        callbackError(context, error);
        console.log(error);
      });
  }

  public isLogged() {
    return this.angularFireAuth.authState;
  }

  redirectUser(auth, router, user) {
    auth.searchUserType2(router, user, function callback(cxt, usrType: firebase.firestore.QuerySnapshot) {
      // console.log('hello', usrType);
      usrType.forEach(i => {
        if (i.ref.parent.id == 'administrator') {
          cxt.navigate(['administrator']);
        } else {
          cxt.navigate(['residentHome/' + i.ref.parent.parent.parent.parent.id + '/' + i.ref.parent.parent.id + '/' + i.ref.id]);
        }
      });
    });
  }

  public getUID(): Promise<any> {
    return new Promise((resolve, reject) => {
      const unsubscribe = firebase.auth().onAuthStateChanged(user => {
        unsubscribe();
        resolve(user);
      }, reject);
    });
  }

  public logOut() {
    this.angularFireAuth.auth
      .signOut()
      .then(response => {
        this.router.navigate(['login']);
      })
      .catch(error => {
        alert('Un error ha ocurrido');
        console.log(error);
      });
  }
}
