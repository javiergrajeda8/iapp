import { TestBed } from '@angular/core/testing';

import { ResidenceTypeService } from './residence-type.service';

describe('ResidenceTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResidenceTypeService = TestBed.get(ResidenceTypeService);
    expect(service).toBeTruthy();
  });
});
