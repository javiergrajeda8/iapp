import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Resident } from '../interfaces/resident';
import { AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResidentService {
  constructor(private firestore: AngularFirestore, private storage: AngularFireStorage) {}

  public saveDocument(imageName, image, data): AngularFireUploadTask {
    return this.storage.ref('resident/' + imageName + '.png').putString(image, data);
  }

  public getDocument(ubication, imageName): Observable<any> {
    return this.storage.ref(ubication + '/' + imageName + '.png').getDownloadURL();
  }

  public get(administratorID, residenceID): AngularFirestoreCollection<unknown> {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident', ref => ref.where('status', '<=', 2));
  }

  public getOne(administratorID, residenceID, residentID): AngularFirestoreDocument<unknown> {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident', ref => ref.where('status', '<=', 2))
      .doc(residentID);
  }

  public set(administratorID, residenceID, resident: Resident) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(resident.uid)
      .set(resident);
      // .then(r => {
      //   callback();
      // })
      // .catch(e => {
      //   alert('Ocurrió un error');
      //   console.log(e);
      // });
  }

  public delete(administratorID, residenceID, residentID, comments) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('residence')
      .doc(residenceID)
      .collection('resident')
      .doc(residentID)
      .update({ status: 99, comments: comments });
  }
}
