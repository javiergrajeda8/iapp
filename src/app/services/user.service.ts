import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  API_ENDPOINT = 'https://ingresapp-jg.firebaseio.com/';
  constructor(private afDB: AngularFireDatabase, private http: HttpClient) { }
}
