import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CC } from '../interfaces/cc';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  constructor(private firestore: AngularFirestore) {}

  get(administratorID) {
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('card');
  }

  set(administratorID, card: CC) {
    if (card.id ===  ''){
      card.id = this.firestore.createId();
    }
    return this.firestore
      .collection('administrator')
      .doc(administratorID)
      .collection('card')
      .doc(card.id)
      .set(card);
  }
}
