import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../services/authorization.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from '../services/custom-validators';
import { Administrator } from '../interfaces/administrator';
import { AdministratorService } from '../services/administrator.service';
import { Router } from '@angular/router';
import { ResidenceService } from '../services/residence.service';
import { Residence } from '../interfaces/residence';
import { User } from '../interfaces/user';
import { Resident } from '../interfaces/resident';
import { ResidentService } from '../services/resident.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registro: any = {};
  rePasswordMessage = '';
  public uid = '';
  residence: Residence = { name: '', code: '', status: 0, type: 0, membership: 1 };
  administratorID;
  residenceID;
  public frmSignup: FormGroup;
  constructor(
    private auth: AuthorizationService,
    private router: Router,
    private fb: FormBuilder,
    private residenceService: ResidenceService,
    private residentService: ResidentService,
    private administratorService: AdministratorService
  ) {}

  ngOnInit() {
    this.frmSignup = this.createSignupForm();
    this.frmSignup.controls['residenceCheckbox'].valueChanges.subscribe(checked => {
      if (checked) {
        this.frmSignup.controls['residenceCode'].setValidators([Validators.required]);
      } else {
        this.residence.name = '';
        this.frmSignup.controls['residenceCode'].setValidators(null);
        this.frmSignup.controls['residenceCode'].setValue('');
      }
      this.frmSignup.controls['residenceCode'].updateValueAndValidity();
    });
  }

  onKey(event: any) {
    // without type info
    this.searchResidenceCode(event.target.value);
  }

  searchResidenceCode(code) {
    try {
      if (code.length > 5) {
        this.residenceService.searchResidence(code).then(result => {
          if (result.size == 1) {
            result.forEach(data => {
              // console.log(data);
              this.residence.name = data.data().name;
              this.residence.type = data.data().type;
              this.residenceID = data.id;
              this.administratorID = data.ref.parent.parent.id;
            });
          } else {
            this.residence.name = '';

            this.frmSignup.controls['residenceCode'].setErrors({ exists: true });
          }
        });
      }
    } catch (e) {}
  }

  register() {
    // this.frmSignup.controls['residenceCode'].value,
    this.auth.register(
      this,
      this.frmSignup.controls['email'].value,
      this.frmSignup.controls['password'].value,
      function success(context, usr: User) {
        if (context.frmSignup.controls['residenceCheckbox'].value && context.frmSignup.controls['residenceCheckbox'] != null) {
          // Residentes
          // console.log('Residente');
          const resident: Resident = {
            uid: usr.uid,
            email: usr.email,
            name: usr.name,
            status: 0,
            residenceCode: context.frmSignup.controls['residenceCode'].value
          };
          context.residentService.set(context.administratorID, context.residenceID, resident, callback => {
            context.router.navigate(['residentHome/' + context.administratorID + '/' + context.residenceID + '/' + resident.uid]);
          });
        } else {
          // Administrador
          const administrator: Administrator = { uid: usr.uid, email: usr.email, status: 1, name: usr.name, membership: 1 };
          context.administratorService.set(administrator);
          context.router.navigate(['administrator']);
        }
      },
      function error(context, error) {
        console.log(error);
        if (error.code === 'auth/email-already-in-use') {
          context.frmSignup.controls['email'].setErrors({ emailUnique: true });
        }
      }
    );
  }

  loginWithFacebook() {
    // console.log(this.frmSignup.controls['residenceCheckbox']);
    this.auth.loginWithFacebook(
      this,
      function success(context, usr: User) {
        // console.log(context.frmSignup.controls['residenceCheckbox'].value);
        if (context.frmSignup.controls['residenceCheckbox'].value && context.frmSignup.controls['residenceCheckbox'] != null) {
          // console.log('Residentes');
          // Residentes
          const resident: Resident = {
            uid: usr.uid,
            email: usr.email,
            name: usr.name,
            status: 0,
            residenceCode: context.frmSignup.controls['residenceCode'].value
          };
          context.residentService.set(context.administratorID, context.residenceID, resident, callback => {
            context.router.navigate(['residentHome/' + context.administratorID + '/' + context.residenceID + '/' + resident.uid]);
          });
        } else {
          // console.log('Administradores');
          // Administradores
          const administrator: Administrator = { uid: usr.uid, email: usr.email, name: usr.name, status: 1, membership: 1 };
          context.administratorService.set(administrator);
          context.router.navigate(['administrator']);
        }
      },
      function error(context, error) {
        if (error.code === 'auth/account-exists-with-different-credential') {
          context.frmSignup.controls['email'].setErrors({ emailUnique2: true });
        }
      }
    );
  }

  loginWithGoogle() {
    this.auth.loginWithGoogle(
      this,
      function success(context, usr: User) {
        if (context.frmSignup.controls['residenceCheckbox'].value && context.frmSignup.controls['residenceCheckbox'] != null) {
          // Residentes
          const resident: Resident = {
            uid: usr.uid,
            email: usr.email,
            name: usr.name,
            status: 0,
            residenceCode: context.frmSignup.controls['residenceCode'].value
          };
          context.residentService.set(context.administratorID, context.residenceID, resident, callback => {
            context.router.navigate(['residentHome/' + context.administratorID + '/' + context.residenceID + '/' + resident.uid]);
          });
        } else {
          // Administradores
          const administrator: Administrator = { uid: usr.uid, email: usr.email, name: usr.name, status: 1, membership: 1};
          context.administratorService.new(administrator);
          context.router.navigate(['administrator']);
        }
      },
      function error(context, error) {
        if (error.code === 'auth/account-exists-with-different-credential') {
          context.frmSignup.controls['email'].setErrors({ emailUnique2: true });
        }
      }
    );
  }

  createSignupForm(): FormGroup {
    return this.fb.group(
      {
        email: [null, Validators.compose([Validators.email, Validators.required])],
        password: [
          null,
          Validators.compose([
            Validators.required,
            // check whether the entered password has a number
            CustomValidators.patternValidator(/\d/, {
              hasNumber: true
            }),
            // check whether the entered password has upper case letter
            CustomValidators.patternValidator(/[A-Z]/, {
              hasCapitalCase: true
            }),
            // check whether the entered password has a lower case letter
            CustomValidators.patternValidator(/[a-z]/, {
              hasSmallCase: true
            }),
            // check whether the entered password has a special character
            CustomValidators.patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, {
              hasSpecialCharacters: true
            }),
            Validators.minLength(8)
          ])
        ],
        confirmPassword: [null],
        residenceCheckbox: [null, null],
        residenceCode: [null, null]
      },
      {
        // check whether our password and confirm password match
        validator: CustomValidators.passwordMatchValidator
      }
    );
  }
}
