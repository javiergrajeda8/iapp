import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthorizationService } from '../services/authorization.service';
import { GuardService } from '../services/guard.service';
import { Guard } from '../interfaces/guard';
import { Residence } from '../interfaces/residence';
import { ResidenceService } from '../services/residence.service';

import { isNumber } from 'util';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-guard',
  templateUrl: './guard.component.html',
  styleUrls: ['./guard.component.scss']
})
export class GuardComponent implements OnInit {
  residenceID: any;
  administratorID: any;
  residence: Residence;
  guards: Guard[];
  guard: Guard = { phoneCountryCode: 0, phone: 0, password: '', uid: '', status: 0, email: '', name: '' };
  closeResult: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private guardService: GuardService,
    private residenceService: ResidenceService
  ) {
    this.administratorID = this.activatedRoute.snapshot.params['uid'];
    this.residenceID = this.activatedRoute.snapshot.params['uid2'];
  }

  ngOnInit() {
    this.getGuards();
    this.getResidenceInfo();
  }

  delete(content, guardName, guardID) {
    this.guard.name = guardName;
    this.guard.uid = guardID;
    this.modalService.open(content, { centered: true });
  }

  confirmDelete(administratorID, residenceID, guardID) {
    this.guardService.delete(administratorID, residenceID, guardID).then(f => {
      this.modalService.dismissAll();
    });
  }

  getGuards() {
    this.guardService
      .get(this.administratorID, this.residenceID)
      .valueChanges()
      .subscribe((data: Guard[]) => {
        this.guards = data;
      });
  }

  getResidenceInfo() {
    this.residenceService
      .get(this.administratorID, this.residenceID)
      .valueChanges()
      .subscribe((data: Residence) => {
        this.residence = data;
      });
  }
}
