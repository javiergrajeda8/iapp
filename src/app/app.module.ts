import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ImageCropperModule } from 'ngx-image-cropper';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { ProcessComponent } from './process/process.component';
import { PriceComponent } from './price/price.component';
import { FaqComponent } from './faq/faq.component';
import { SupportComponent } from './support/support.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizationService } from './services/authorization.service';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { MyGuardService } from './services/my-guard.service';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AdministratorHomeComponent } from './administrator-home/administrator-home.component';
import { GuardComponent } from './guard/guard.component';
import { ConstantService } from './services/constant.service';
import { GuardFormComponent } from './guard-form/guard-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DigitOnlyDirective } from './directive/digit-only.directive';
import { ResidentComponent } from './resident/resident.component';
import { ResidentFormComponent } from './resident-form/resident-form.component';
import { ResidentHomeComponent } from './resident-home/resident-home.component';
import { LoginForgetPasswordComponent } from './login-forget-password/login-forget-password.component';
import { NewVisitComponent } from './new-visit/new-visit.component';
import { NewVisitCodeComponent } from './new-visit-code/new-visit-code.component';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { NewVisitRecurrentComponent } from './new-visit-recurrent/new-visit-recurrent.component';
import { NewVisitPendingComponent } from './new-visit-pending/new-visit-pending.component';
import { LoginGuardComponent } from './login-guard/login-guard.component';
import { GuardHomeComponent } from './guard-home/guard-home.component';
import { GuardEnterCodeComponent } from './guard-enter-code/guard-enter-code.component';
import { ReportVisitDayComponent } from './report-visit-day/report-visit-day.component';
import { ReportVisitWeekComponent } from './report-visit-week/report-visit-week.component';
import { CodeCardComponent } from './code-card/code-card.component';
import { AdministratorConfigurationComponent } from './administrator-configuration/administrator-configuration.component';
import { ResidentConfigurationComponent } from './resident-configuration/resident-configuration.component';
import { AdministratorAdministrationComponent } from './administrator-administration/administrator-administration.component';
import { AdministratorReportsComponent } from './administrator-reports/administrator-reports.component';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import { ToastsContainerComponent } from './toast-container/toast-container.component';
import { ResidentAdministrationComponent } from './resident-administration/resident-administration.component';
import { ResidentReportsComponent } from './resident-reports/resident-reports.component';
import { NgbdSortableHeader } from './directive/sortable.directive';
import { NgbdTableComplete } from './test/test.component';

const firebaseConfig = {
  apiKey: 'AIzaSyCCtejiJvqg3n0Xf76CDhJQ7lCoOLz1iY0',
  authDomain: 'ingresapp-jg.firebaseapp.com',
  databaseURL: 'https://ingresapp-jg.firebaseio.com',
  projectId: 'ingresapp-jg',
  storageBucket: 'ingresapp-jg.appspot.com',
  messagingSenderId: '327251504047',
  appId: '1:327251504047:web:7946057a40aee3412f9d5a'
};

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'administrator', component: AdministratorHomeComponent, canActivate: [MyGuardService] },
  { path: 'administratorConfiguration/:uid/:uid2', component: AdministratorConfigurationComponent, canActivate: [MyGuardService] },
  { path: 'administratorAdministration/:uid', component: AdministratorAdministrationComponent, canActivate: [MyGuardService] },
  { path: 'administratorReports/:uid/:uid2', component: AdministratorReportsComponent, canActivate: [MyGuardService] },
  { path: 'resident/:uid/:uid2', component: ResidentComponent, canActivate: [MyGuardService] },
  { path: 'residentHome/:uid/:uid2/:uid3', component: ResidentHomeComponent, canActivate: [MyGuardService] },
  { path: 'residentConfiguration/:uid/:uid2/:uid3', component: ResidentConfigurationComponent, canActivate: [MyGuardService] },
  { path: 'residentAdministration/:uid/:uid2/:uid3', component: ResidentAdministrationComponent, canActivate: [MyGuardService] },
  { path: 'residentForm/:uid/:uid2/:uid3', component: ResidentFormComponent, canActivate: [MyGuardService] },
  { path: 'residentReports/:uid/:uid2/:uid3', component: ResidentReportsComponent, canActivate: [MyGuardService] },
  { path: 'newVisit/:uid/:uid2/:uid3/:uid4', component: NewVisitComponent, canActivate: [MyGuardService] },
  { path: 'newVisitRecurrent/:uid/:uid2/:uid3', component: NewVisitRecurrentComponent, canActivate: [MyGuardService] },
  { path: 'newVisitPending/:uid/:uid2/:uid3', component: NewVisitPendingComponent, canActivate: [MyGuardService] },
  { path: 'visitorCode/:uid/:uid2/:uid3/:uid4', component: NewVisitCodeComponent, canActivate: [MyGuardService] },
  { path: 'codeCard/:uid/:uid2/:uid3', component: CodeCardComponent, canActivate: [MyGuardService] },
  { path: 'guard/:uid/:uid2', component: GuardComponent, canActivate: [MyGuardService] },
  { path: 'guardForm/:uid/:uid2/:uid3', component: GuardFormComponent, canActivate: [MyGuardService] },
  { path: 'guardEnterCode/:uid/:uid2/:uid3', component: GuardEnterCodeComponent, canActivate: [MyGuardService] },
  { path: 'guardHome/:uid/:uid2/:uid3', component: GuardHomeComponent, canActivate: [MyGuardService] },
  { path: 'reportVisitDay/:uid/:uid2/:uid3', component: ReportVisitDayComponent, canActivate: [MyGuardService] },
  { path: 'reportVisitWeek/:uid/:uid2/:uid3', component: ReportVisitWeekComponent, canActivate: [MyGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'loginGuard', component: LoginGuardComponent },
  { path: 'forget-password', component: LoginForgetPasswordComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'process', component: ProcessComponent },
  { path: 'support', component: SupportComponent },
  { path: 'price', component: PriceComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'test', component: NgbdTableComplete }
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    ProcessComponent,
    PriceComponent,
    FaqComponent,
    SupportComponent,
    LoginComponent,
    RegisterComponent,
    AdministratorHomeComponent,
    GuardComponent,
    GuardFormComponent,
    DigitOnlyDirective,
    ResidentComponent,
    ResidentFormComponent,
    ResidentHomeComponent,
    LoginForgetPasswordComponent,
    NewVisitComponent,
    NewVisitCodeComponent,
    NewVisitRecurrentComponent,
    NewVisitPendingComponent,
    LoginGuardComponent,
    GuardHomeComponent,
    GuardEnterCodeComponent,
    ReportVisitDayComponent,
    ReportVisitWeekComponent,
    CodeCardComponent,
    AdministratorConfigurationComponent,
    ResidentConfigurationComponent,
    AdministratorAdministrationComponent,
    AdministratorReportsComponent,
    ToastsContainerComponent,
    ResidentAdministrationComponent,
    ResidentReportsComponent,
    NgbdSortableHeader,
    NgbdTableComplete
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    ImageCropperModule,
    AngularFireStorageModule,
    CreditCardDirectivesModule
  ],
  providers: [AuthorizationService, MyGuardService, ConstantService],
  bootstrap: [AppComponent]
})
export class AppModule {}
